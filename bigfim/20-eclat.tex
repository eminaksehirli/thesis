\section{Preliminaries}
Let $\I = \{i_{1}, i_{2}, \dots, i_{n}\}$ be a set items, a transaction is defined as $\T = (\textit{tid}, X)$ where \tid is a transaction identifier and $X$ is a set of items over $\I$.
A transaction database $\DB$ is a set of transactions.
Its vertical database $\DB'$ is a set of pairs that are composed of an item and the set of transactions $\tidlist_{j}$ that contain the item:
%
$$
\DB' = \{ (i_{j}, \tidlist_{i_j} = \{ \tid \ie i_{j} \in X, (\tid, X) \in \DB \} ) \}.
$$
$\tidlist_{i_j}$ is also called the \emph{cover} or \emph{tid-list} of $i_{j}$.

The support of an itemset $Y$ is the number of transactions that
%is a superset of
contain the itemset. Formally,
\[
\supp(Y) = | \{ \tid \ie Y \subseteq X, (\tid, X) \in \DB \} |
\]
or, in vertical database format
\[
\supp(Y) = | \bigcap_{i_{j} \in Y} \tidlist_{i_j} |.
\]

An itemset is said to be \emph{frequent} if its support is greater than a given threshold $\sigma$, which is called \emph{minimum support} or \emph{\minsup} in short.
Frequency is a monotonic property w.r.t. set inclusion, meaning that if an itemset is not frequent, none of its supersets are frequent.
Similarly, if an itemset is frequent, all of its subsets are frequent.

Items in an itemset can be represented in a fixed order; without loss of generality let us assume that items in itemsets are always in the same order as they are in $\I$, i.e., $Y = \{i_{a}, i_{b},\dots, i_{c} \} \Leftrightarrow a < b < c$.
Then, the common $k$-\emph{prefix} of two itemsets are the first $k$ elements in those sets that are the same,
e.g., sets $Y = \{i_{1}, i_{2}, i_{4} \}$ and $X = \{i_{1}, i_{2}, i_{5}, i_{6} \}$ have a common 2-\emph{prefix} of $\{i_{1}, i_{2}\}$.
A \emph{prefix tree} is a tree structure where each path represents an itemset, which is exactly the path from the root to the node, and sibling nodes share the same prefix.
Note that all the possible itemsets in $\I$, i.e., the power set of $\I$, can be expressed as a prefix tree.

\emph{Projected} or \emph{conditional} database of an item $i_{a}$ is the set of transactions in $\DB$ that includes $i_{a}$.

Apriori~\cite{apriori} starts with finding the frequent items by making a pass over $\DB$ to count them.
Then, it combines these frequent items to generate \emph{candidate itemset}s of length 2, and counts their supports by making another pass over $\DB$, removing infrequent candidates.
The algorithm iteratively continues to extend $k$-length candidates by one item and counts their supports by making another pass over $\DB$ to check whether they are frequent.
Exploiting the monotonic property, Apriori prunes those candidates for which a subset is known to be infrequent.
Depending on the minimum support threshold used, this greatly reduces the search space of candidate itemsets.

Eclat~\cite{zaki_parallel_1997} traverses the prefix tree in a depth first manner to find the frequent itemsets.
The monotonic property states that if an itemset, a path in the prefix tree, is infrequent then all of its subtrees are infrequent.
Thus, if an itemset is found to be infrequent, its complete subtree is immediately pruned.
If an itemset is frequent then it is treated as a prefix and extended by its immediate siblings to form new itemsets.
This process continues until the complete tree has been traversed.
Eclat uses a vertical database format for fast support computation. Therefore, it needs to store $\DB'$ in main memory.


MapReduce~\cite{dean_mapreduce:_2004} is a parallel programming framework that provides a relatively simple programming interface together with a robust computation architecture.
MapReduce programs are composed of two main phases.
In the \emph{map} phase, each mapper processes a distinct chunk of the data and produces key-value pairs.
In the \emph{reduce} phase, key-value pairs from different mappers are combined by the framework and fed to reducers as pairs of key and value lists.
Reducers further process these intermediate parts of information and output the final results.
