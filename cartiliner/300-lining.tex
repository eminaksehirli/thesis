\section{Efficient Mining\label{sec:mining}}
In this section we introduce our proposed algorithm \clon, which efficiently finds the subspace clusters by using the ordered neighborhoods.
%the details can be found in Section~\ref{sec:sup-algorithms} (in supplementary material).
\clon takes two parameters, (1) Minimum size of a cluster \textit{MinSize} and (2) Neighborhood size \textit{k},
and outputs all the subspace clusters in the database.

\subsection{Transformation}
In its first phase, \clon converts the relational database into neighborhood data\-bases.
Neighborhood databases are created for each of the projections (dimensions).
Values in each dimension has to be sorted only once, then the neighborhoods can be calculated very fast by using a sliding window.
A neighborhood DB is created for each of the projections or each of the (dis)similarity measures.
Algorithm~\ref{alg:create-neigh} shows the process of creating neighborhood DBs.

\begin{algorithm}[htb]
	\begin{algorithmic}[1]
		\REQUIRE \DB: Real valued high dimensional database, $k$: Neighborhood size
		\ENSURE $\mathcal{T}$: Neighborhood databases

		\STATE $\mathcal{T} \assign [\,]$
	
		\FORALL{$\DB^{A_{i}} \in \DB$}
			\STATE sort $\DB^{A_{i}}$ in ascending order
			\STATE $\ND^{A_i} \assign [\,]$

			\FORALL{$\vo \in \DB^{A_{i}}$}
				\STATE append $\knn{k}{\vo_i}$ to $\ND^{A_i}$
			\ENDFOR

			\STATE $\mathcal{T}_{i} \assign \ND^{A_{i}}$
		\ENDFOR
		\RETURN
	\end{algorithmic}
	\caption{Create neighborhood databases}
	\label{alg:create-neigh}
\end{algorithm}

\clon already benefits from the ordered neighborhoods for the neighborhood DB creation.
It exploits the consecutiveness properties to store the neighborhood information
so that, instead of keeping the whole neighborhoods, only the start and end of the neighborhoods are kept.
This provides the algorithm a dramatic saving on the memory.

The neighborhood databases are in horizontal format, meaning that they are composed of the neighborhoods.
However, the consequent steps of the algorithm need the data in vertical format for efficient computation,
which is composed of objects and the neighborhoods that they occur in.
Since both the objects and the neighborhoods are continuous, according to the Properties \ref{pro:cons-obj} and \ref{pro:cons-neigh}, we only require the starting neighborhood, that is the id of the neighborhood in which the object first appears,
and the end neighborhood, that is the id of the neighborhood in which the object appears for the last time.
To avoid confusion, we make the following definition:
\textit{item} is
an associative array of an object reference \textit{id}, starting neighborhood id \textit{txS} and the end neighborhood id \textit{txE}.

\subsection{Finding the Neighborhoods in Projections}
Second phase is about finding the clusters in individual projections.
As Theorem~\ref{theo:square} states, clusters are the recurrent object sets in the neighborhood DB.
\clon looks for object sets that are large and recurrent enough to satisfy \textit{MinSize}.
These object sets can be found in linear (with the number of objects) time by exploiting the properties of the ordered neighborhoods.
In this phase, \clon identifies all of the recurrent objects sets in individual projections as one dimensional clusters.

Algorithm~\ref{alg:cartimaxi} shows the \maxiband algorithm, our efficient maximal recurrent object set miner that we use to find one dimensional clusters.
Since each subset of a cluster is also a cluster, we search for maximally recurrent object sets to minimize the redundant clusters.

\maxiband takes a sorted list of items $\mathcal{I}$ and a minimum length $\lambda$ as input.
%Each item $i \in \mathcal{I}$ is associated with a reference to object it represents and the list of transactions that the item appears.
%However, since the transactions in neighborhood DBs are consecutive, we do not have to store all the transactions.
%We only require the starting transaction, id of the transaction that the item first appears, and the end transaction, id of the transaction that the item appears for the last time.
%Therefore, each item is an associative array of an object reference \textit{id}, starting transaction id \textit{txS} and the end transaction id \textit{txE}.
%\maxiband searches for the large object sets that recurrently co-occur in the neighborhoods.
$\lambda$ parameter is used to determine how large and recurrent a set of objects should be.
Note that, since the clusters form square structures, only one parameter is enough.
To compute in how many neighborhoods a set of objects occur together, we use the support function $\sigma$,
which returns the number of neighborhoods a set of objects occur.
Note that $\sigma$ exploits the consecutiveness properties of the neighborhood database (line 4).
This support computation is visualized in Figure~\ref{fig:support}, which shows 5 consecutive items with their transaction lists.
Start transaction and end transaction of the items $i_{o}$ and $i_p$ are marked with \textit{txS} and \textit{txE}.
Support of the itemset $\{i_o, i_a, i_b, i_c, i_p\}$ is the height of the orange rectangle, which is $|i_o.\textit{txE} - i_p.\textit{txS}|$.
Similarly, height of the green rectangle is the support of the itemset $\{i_a, i_b, i_c\}$.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.3\columnwidth,keepaspectratio=true,draft=false]{figs/support}
	\end{center}
	\caption{Heights of the rectangles are the supports of the respective itemsets}
	\label{fig:support}
\end{figure}

\maxiband scans through a neighborhood DB with an elastic frame to find all of the maximal recurrent object sets.
It starts with creating an object set from the first $\lambda$ objects (lines 2--3),
and it is extended until non of its supersets are recurrent (lines 6--7).
If the object set itself is recurrent, then it is added to the list of maximal recurrent object sets (lines 8--9).
Since the order of the objects is fixed and the object sets are always continuous we only keep the first and last objects.
%\maxiband tries to maximize each object set by adding the next object if the new object set will still be frequent (lines 6--7).
If the first object of the shifted frame does not add any additional neighborhoods, it skips the object (lines 11--12).
It shifts frame by 1 item (lines 13--15) and continues until it scans the whole database (line 5).


\begin{algorithm}
	\begin{algorithmic}[1]
		\REQUIRE $\mathcal{I}$: sorted items, $\lambda$: minumum size
		\ENSURE $\mathcal{F}$: maximal recurrent object sets

		\STATE $\mathcal{F} \assign \emptyset$
		\STATE $s \assign 0$ \COMMENT{Index of the first item of the itemset}
		\STATE $e \assign s + \lambda$ \COMMENT{Index of the last item of the itemset}
		\STATE $\sigma: \mathcal{I}^2 \rightarrow \mathbb{N}^+, \sigma(i_{a}, i_{b}) = i_{a}.\textit{txE} - i_{b}.\textit{txS}$
		\WHILE{$e < \textit{len}(\mathcal{I})$}
			%\STATE $s \assign i_{s}.txE - i_{e}.txS$

			\WHILE{$\sigma(i_{s}, i_{e+1}) > \lambda$}
				\STATE $e \assign e + 1$
			\ENDWHILE
			
			\IF{$\sigma(i_{s}, i_{e}) > \lambda$}
				\STATE $\mathcal{F} \assign \mathcal{F} \cup (i_{s}, i_{e})$
				\STATE $e \assign e + 1$
			\ENDIF

			\WHILE{$i_{s+1}.\textit{txE} = i_{s}.\textit{txE}$}
				\STATE $s \assign s + 1$
			\ENDWHILE

			\STATE $s \assign s + 1$
			\IF{$e - s < \lambda$}
				\STATE $e \assign s + \lambda$
			\ENDIF
		\ENDWHILE
	\end{algorithmic}
	\caption{\maxiband: Maximal frequent itemset miner}
	\label{alg:cartimaxi}
\end{algorithm}



\subsection{Finding Subspace Clusters}
In the third and the last phase, one dimensional clusters are used as a base to find higher dimensional clusters.
\clon conducts a deep-first search on dimensions to find out whether the one dimensional clusters are supersets of higher dimensional clusters.
We exploit properties of ordered neighborhoods for this phase to speed-up the computation.
Lastly, the recurrent object sets are output as clusters along with the dimensions that they are recurrent in.


Algorithm~\ref{alg:whole} shows the \clon algorithm that finds all the subspace clusters.
It takes a numeric high dimensional database $\DB$, a cluster size $\lambda$, and a neighborhood size $k$ as inputs
and outputs all the subspace clusters in $\DB$.
A subspace cluster is represented as a tuple, composed of a set of objects and a set of relevant dimensions.
The algorithm starts with converting the \DB into the neighborhood DBs (line 4).
Then, the maximal recurrent objects sets in each neighborhood DB is found (line 5--6).
Note that, instead of directly passing $\lambda$ as an argument to \maxiband, we pass the maximum of $\lambda$ and $k \times 0.6$.
Because the baseline property, Property~\ref{pro:baseline}, states that there is always a minimum amount of recurrency in a neighborhood DB although there are no cluster formations.
Computing this value reveals that if there are no cluster structures in the data, almost all of the neighborhoods satisfy $\lambda = k\times0.5$.
If the $\lambda$ value is lower than this value, then there will be too many false positive clusters in the output.
Therefore, we empirically select $k \times 0.6$ as the minimum recurrency threshold to eliminate the insignificant recurrencies.
%In case of a too high $k$ or a too low $\lambda$, this check ensures only the meaningful clusters are found.
Further discussion of the parameter selection is in Section~\ref{sec:parameter}.

Each recurrent object set in an individual neighborhood DB is a 1 dimensional cluster (line 8),
which can possibly be a superset of a cluster in a higher dimensional space.
Therefore, to further process these clusters, we keep them along with their known dimensions and possible extensions (line 9).
We keep them in a stack to employ a deep-first search and thus minimize the memory requirement.

Second part of the algorithm refines the clusters by checking their possible extensions in higher dimensions (lines 10--18).
Search space within a neighborhood DB of a possible dimension extension is limited to the objects in the cluster (line 13--14).
Each maximal recurrent object set that is a subset of these objects is a cluster in this dimension in addition to the dimensions of the starting cluster (line 16).
%For a possible dimension extension, it gets the items that represent the objects in the cluster (line 13), 
%and then, it mines the maximal frequent itemsets that are subsets of these items (line 14).
Newly found clusters are added both to the found cluster list and to the stack for further refinement (lines 15--17).

\begin{algorithm}[tb]
	\begin{algorithmic}[1]
		\REQUIRE \DB: Numeric high dimensional database, $\lambda$: Cluster size, $k$: Neighborhood size
		\ENSURE $\mathcal{C}$: Subspace clusters

		\STATE $\mathcal{C} \assign \emptyset$
		\STATE initialize empty stack $\mathcal{S}$
		\STATE $A = \{1, 2, \cdots, |\Ats|\}$
		\STATE $\mathcal{T} \assign$ Convert \DB to neighborhood DBs
		\FORALL{$\ND^{A_i} \in \mathcal{T}$}
			\STATE $F \assign$ \maxiband$(\mathcal{I} \in \ND^{A_{i}}, \max(\lambda, k\times0.6))$
			\FORALL{$f \in F$}
				\STATE $\mathcal{C} \assign \mathcal{C} \cup (f, \{i\})$
				\STATE push $(f, \{i\}, A  - \{i\})$ to $\mathcal{S}$
			\ENDFOR
		\ENDFOR
		\REPEAT
		\STATE $(f, D, P) \assign$ pop $\mathcal{S}$
		\FORALL{$p \in P$}
			\STATE $I' \assign$ get ordered items of $f$ from $\ND^{A_p}$
			\STATE $F \assign$ \maxiband$(I', \lambda)$
			\FORALL{$f' \in F$}
				\STATE $\mathcal{C} \assign \mathcal{C} \cup (f, D \cup \{p\})$
				\STATE push $(f, D \cup \{p\}, P - \{p\})$ to $\mathcal{S}$
			\ENDFOR
		\ENDFOR
		\UNTIL{$\mathcal{S}$ is empty}
	\end{algorithmic}
	\caption{\clon Algorithm}
	\label{alg:whole}
\end{algorithm}



%Instead of keeping the neighborhoods in memory, we keep the neighborhood information of the individual objects.
%	We define the \textit{item} as a data structure that represents an object in a neighborhood DB.
%	It is an associative array of an object id \textit{ID}, the neighborhood that the object first appears \textit{NS}, and the neighborhood that the object last appears \textit{NE}.
%Each object appear in a consecutive list of neighborhoods (Property \ref{pro:cons-neigh}).




%The neighborhood databases are in horizontal format, meaning that they are composed of the neighborhoods.
%However, the consequent steps of the algorithm need the data in vertical format for efficient computation, 
%which is composed of objects and the neighborhoods that they occur.
%Since both the objects and the neighborhoods are continuous, according to Properties \ref{pro:cons-obj} and \ref{pro:cons-neigh}, we only require the starting neighborhood, that is the id of the neighborhood that the object first appears,
%and the end neighborhood, that is the id of the neighborhood that the object appears for the last time.
%Therefore, we can define each object as an associative array of an object reference \textit{id}, starting neighborhood id \textit{txS} and the end neighborhood id \textit{txE}.
%
%\begin{algorithm}
%	\begin{algorithmic}[1]
%		\REQUIRE \DB: Real valued high dimensional database, $k$: Neighborhood size
%		\ENSURE $\mathcal{T}$: Neighborhood databases
%
%		\STATE $\mathcal{T} \assign [\,]$
%	
%		\FORALL{$A_{i} \in \DB$}
%			\STATE sort $A_{i}$ in ascending order
%			\STATE $\ND^{A_i} \assign [\,]$
%
%			\FORALL{$\vo_{i} \in A_{i}$}
%				\STATE $\ND^{A_i}  \assign \ND^{A_i} \cup \knn{k}{\vo_i}$
%			\ENDFOR
%
%			\STATE $\mathcal{T}_{i} \assign \ND^{A_{i}}$
%		\ENDFOR
%		\RETURN
%	\end{algorithmic}
%	\caption{Create neighborhood databases}
%	\label{alg:create-neigh}
%\end{algorithm}


%\subsection{Neighborhood Database}
%As can be seen on Figure~\ref{fig:toy-k}, recurrent neighborhoods of the projected databases form rectangular areas on the diagonal.
%These kind of formations are called \textit{banded} structures.
%They are visually interpretable and they have some intrinsic properties that can exploited to improve mining efficiency.
%Let us have a look at some important properties of a sorted neighborhood database of a 1D projection:
%\begin{itemize}
%	\item Consecutive itemsets:
%Let three items $i_{a}$, $i_{b}$, and $i_{c}$. 
%If $a<b<c$, then $\sigma(\{i_{a}, i_{b}\}) = \sigma(\{i_{a}, i_{b}, i_{c}\})$.
%Therefore, support of an itemset is equal to support of the union of their first and last items.
%	\item Consecutive transactions: Let $i$ be an item and $t_{a}$, $t_{b}$, and $t_{c}$ be three transactions. 
%If $a<b<c$, then $i \in t_{a} \wedge i \in t_{c} \implies i \in t_{c}$.
%Therefore, supoprt of an itemset can be computed by finding the first and last transaction it appears.
%\end{itemize}
%
%%As can be seen on Figure~\ref{fig:toy-k} 
%%%and shown in Lemma~\ref{lem:projection} and Lemma~\ref{lem:consecutive}, 
%%neighborhoods of projected databases form \textit{banded} structures~\cite{banded}.
%
%A rectangular area of the neighborhood DB represents an itemset, where
%Combining these two properties, problem of finding a maximal frequent itemset
%is reduced to finding a rectangular area with a height.
%To elaborate,  a rectangular region of a sorted neighborhood database defines
%
%Property: Baseline: If the data is uniformly distributed, then the neighborhoods does not form any significant bands.
%
%
%Instead of employing frequent itemset mining methods we can exploit other properties of the neighborhoods.
%Recurrent, thus frequent, neighborhoods form rectangular shapes;
%as shown in Lemma~\ref{lem:projection} and Lemma~\ref{lem:consecutive} and as is visible in Figure~\ref{fig:toy-k}.
%Therefore, finding rectangles in the neighborhoods plot is same as finding the frequent itemsets.
%Moreover, when neighborhood size is fixed for all of the objects, then the finding rectangles can be reduced to a simpler linear problem.


%\subsection{Finding the Neighborhoods in Projections}
%\subsection{Finding Recurrent Object Sets}
%
%To find the cluster structures, we would like to find \textit{recurrent object set}s that are larger than a certain size.
%We call an object set \textit{recurrent} if it is recurrent more than a certain threshold.
%A \textit{maximal recurrent object set} is a recurrent object set that non of its proper supersets are recurrent.
%Recurrency is the support of an itemset and the height of a rectangle,
%while the minimum size is the length of the itemset and the width of a rectangle.

%Algorithm~\ref{alg:cartimaxi} shows the \maxiband algorithm, our efficient maximal recurrent object set miner.
%\maxiband takes a sorted list of items $\mathcal{I}$ and a minimum length $\lambda$ as input.
%%Each item $i \in \mathcal{I}$ is associated with a reference to object it represents and the list of transactions that the item appears.
%%However, since the transactions in neighborhood DBs are consecutive, we do not have to store all the transactions.
%%We only require the starting transaction, id of the transaction that the item first appears, and the end transaction, id of the transaction that the item appears for the last time.
%%Therefore, each item is an associative array of an object reference \textit{id}, starting transaction id \textit{txS} and the end transaction id \textit{txE}.
%%\maxiband searches for the large object sets that recurrently co-occur in the neighborhoods.
%$\lambda$ parameter is used to determine how large and recurrent a set of objects should be.
%Note that, since the clusters form square structures, only one parameter is enough.
%To compute in how many neighborhoods a set of objects occur together, we use support function $\sigma$,
%which returns the number of neighborhoods a set of objects occur. % by exploiting the consecutiveness properties (line 4).
%%which exploits the consecutiveness property of the neighborhood database (line 4) to find 
%%This support computation is visualized in Figure~\ref{fig:support}, which shows 5 consecutive items with their transaction lists.
%%Start transaction and end transaction of the items $i_{o}$ and $i_p$ are marked with \textit{txS} and \textit{txE}.
%%Support of the itemset $\{i_o, i_a, i_b, i_c, i_p\}$ is the height of the orange rectangle, which is $|i_o.\textit{txE} - i_p.\textit{txS}|$.
%%Similarly, height of the green rectangle is the support of the itemset $\{i_a, i_b, i_c\}$.
%%
%%\begin{figure}[tb]
%%	\begin{center}
%%		\includegraphics[width=0.6\columnwidth,keepaspectratio=true,draft=false]{figs/support}
%%	\end{center}
%%	\caption{Heights of the rectangles are the supports of the respective itemsets}
%%	\label{fig:support}
%%\end{figure}
%
%\maxiband scans through a neighborhood DB with an elastic frame to find all of the maximal recurrent object sets.
%It starts with creating an object set from the first $\lambda$ objects (lines 2--3),
%and it is extended until non of its supersets are recurrent (lines 6--7).
%If the object set itself is recurrent, then it is added to the list of maximal recurrent object sets (lines 8--9).
%%Since the order of the items is fixed and itemsets are always continuous we only keep the first and last item of the itemset.
%
%
%
%It tries to maximize each object set by adding the next object if the new object set will still be frequent (lines 6--7).
%If the first object of the shifted frame does not add any additional neighborhoods, it skips the object (lines 10--11).
%It shifts frame by 1 item (lines 12--13) and continues until it scans the whole database (line 5).
%
%
%%a starting transaction and an end transaction which are the ids of the transaction that this item first appear and the last transaction that the item last appears, respectively.
%
%%The method we propose to find recurrent neighborhoods is given in Algorithm~\ref{algo:cartimaxi}.
%%Maximizer takes the items which are sorted by the works on a sorted list of items. 
%%
%%Finding rectangular areas with a minimum height and minimum width 
%
%\begin{algorithm}
%	\begin{algorithmic}[1]
%		\REQUIRE $\mathcal{I}$: sorted items, $\lambda$: minumum size
%		\ENSURE $\mathcal{F}$: maximal frequent itemsets
%
%		\STATE $\mathcal{F} \assign \emptyset$
%		\STATE $s \assign 0$ \COMMENT{Index of the first item of the itemset}
%		\STATE $e \assign s + \lambda$ \COMMENT{Index of the last item of the itemset}
%		\STATE $\sigma: \mathcal{I}^2 \rightarrow \mathbb{N}^+, \sigma(i_{a}, i_{b}) = i_{a}.\textit{txE} - i_{b}.\textit{txS}$
%		\WHILE{$e < \textit{len}(\mathcal{I})$}
%			%\STATE $s \assign i_{s}.txE - i_{e}.txS$
%
%			\WHILE{$\sigma(i_{s}, i_{e+1}) > \lambda$}
%				\STATE $e \assign e + 1$
%			\ENDWHILE
%			
%			\IF{$\sigma(i_{s}, i_{e}) > \lambda$}
%				\STATE $\mathcal{F} \assign \mathcal{F} \cup (i_{s}, i_{e})$
%				\STATE $e \assign e + 1$
%			\ENDIF
%
%			\WHILE{$i_{s+1}.\textit{txE} = i_{s}.\textit{txE}$}
%				\STATE $s \assign s + 1$
%			\ENDWHILE
%
%			\STATE $s \assign s + 1$
%			\IF{$e - s < \lambda$}
%				\STATE $e \assign s + \lambda$
%			\ENDIF
%		\ENDWHILE
%	\end{algorithmic}
%	\caption{\maxiband: Maximal frequent itemset miner}
%	\label{alg:cartimaxi}
%\end{algorithm}


%\subsection{Finding subspace clusters\label{sec:finding-clusters}}



%\begin{definition}[Neighborhood Curve]
%Assume that we mark the center of each neighborhood with a point,
%and we connect these points from the top to the bottom to create a curve.
%We call this curve the \textit{neighborhood curve}.
%\end{definition}
%Figure~\ref{fig:toy-curves} shows the neighborhood curve for the neighborhood of size 6.
%
%\begin{figure}[ht]
%	\begin{center}
%		\includegraphics[width=0.9\columnwidth,keepaspectratio=true,draft=false]{figs/toy-k6-curves-both.png}
%	\end{center}
%	\caption{Creating the neighborhood curve by combining the centers of the neighborhoods.}
%	\label{fig:toy-curves}
%\end{figure}


%Steep segments on a neighborhood curve correspond to rectangular formations on neighborhoods plot and hence cluster structures in the data.
%Therefore, we look for segments that are steeper than a certain threshold.
%Moreover, since small clusters are not interesting, we only search for clusters that are larger than a certain size.
%
%If there are not any cluster structures in a projection, then neighborhood curve will be very smooth because of non-recurrent neighborhoods.
%Figure~\ref{fig:uniform} shows an extreme case, for an area where the objects are distributed uniformly, i.e., the distance between each pair of consequent objects are the same, 
%neighborhood curve is a straight line which is parallel to the line $x=-y$.
%Therefore, line segments that are steeper than the tangent of the line $x=-y$, are indicators of cluster structures.
%
%\begin{definition}[\lining]
%	Process of finding the segments that are longer than a certain \minlen and steeper than a certain \mintan is called \lining.
%	And these segments are called \textit{steep segments}.
%\end{definition}
%
%Obviously, the neighborhoods should be sorted properly before \lining,
%which is easy for a one dimensional projection.
%%For a one dimensional projection this is easy. 
%We sort the objects according to their projected values first, 
%then use this order to create neighborhood plot and neighborhood curve.
%Computational complexity of finding all the steep segments on a neighborhood curve is $O(n)$ where $n$ is the number of objects.
%Combining with the sorting, complexity becomes $O(n + n \log(n))$.
%
%
%\lining identifies the areas that has a similar density by using the neighborhood information.
%Therefore, it has an advantage of being agnostic to the actual density of the area.
%
%
%\subsection{Finding Subspace Clusters}
%By definition, subspace clusters forms structures only in subsets of attributes.
%Therefore, we have to conduct a comparative search in combinations of attributes to find the clusters \textit{along with} their attributes,
%which makes the already NP-hard problem of clustering even more complex.
%Computing the similarities for each combination of attributes is proposed in the literature.
%However, when the number of attributes increase, this approach becomes intractable.
%
%
%%To find the clusters that exist in multiple dimensions search space should be extended.
%Some other methods propose to use lower level projections to find the clusters in higher number of dimensions~\cite{cartification,FIRES}.
%CartiClus~\cite{cartification}, an algorithm that also uses the local neighborhood information, 
%achieve this by concatenating neighborhoods from multiple dimensions and looking for recurrencies in the merged data.
%However, concatenation requires that the ordering of the objects should be the same for all the neighborhood plots, and thus \lining cannot be used.
%Although frequent itemset mining can yield to good results, because of its complexity, finding all the cluster structures is not feasible.
%
%%\textcolor{red}{Also mention, search by combining real dimensions maybe?}
%
%Fortunately, we can exploit the properties of transaction database to speed up the process.
%\cartiliner, first finds the steep segments by \lining the individual 1-D projections.
%Then, the neighborhoods are concatenated to a one large transaction database.
%Supports of these steep segments are counted in this transaction database and the ones that are still frequent are output as subspace clusters, 
%while the projections which they are frequent becomes the dimensions of a cluster.

%\begin{algorithm}
% \caption{\cartiliner Algorithm}
% \label{alg:cartiliner}
%	\begin{algorithmic}
%		\REQUIRE \DB
%		\ENSURE Subspace clusters
%		\STATE $\mathcal{C} \assign \emptyset$ \COMMENT{Object Clusters}
%		\FORALL{$\DB^{i} \in \DB$}
%			\STATE $\mathcal{C} \assign \mathcal{C} \cup \lining(\DB^{i})$
%		\ENDFOR
%
%	\end{algorithmic}
%\end{algorithm}

\subsection{Complexity Analysis}
For a numeric \DB that had $n$ objects and $d$ attributes, computational complexity of transforming it to the neighborhood DB is $O(d\times n \times \log(n) \times k)$.
And, thanks to the consecutiveness properties we do not have to store the whole data, only the start and end of the neighborhoods are enough.
Therefore, space need for the neighborhood DBs is $O(n \times d)$ which is equivalent to the storage need of the original \DB.

Exploiting the properties of the neighborhood DB allows us to find one dimensional clusters extremely efficiently, in $O(n)$.
%\maxiband mines all the maximal frequent itemsets in a neighborhood DB in $O(n)$.
Therefore, complexity of finding all the clusters in all the projected databases is $O(d \times n)$.

Execution time for \clon to find all of the subspace clusters  is highly dependent on the characteristics of the data.
Section~\ref{sec:liner:experiments} shows the experiments that we conduct on various kinds of data to empirically evaluate the effectiveness of our method.


%\textcolor{red}{TODO:
%\begin{itemize}
%	%\item \textcolor{red}{How does \lining works with non-separable clusters!!!}
%	\item Creating hierarchy of objects by \lining multiple times. Cophenetic distance?
%	\item buffering
%	\item Post-processing
%\end{itemize}
%}






%And since frequent itemset mining methods are not fast finding all Frequent itemset mining algorithms are 
%
%cannot use \lining because the ordering
%fallback to fis
%
%one should fallback 
%we can not exploit \linin
%However, to employ frequent itemset methods, all of the neighborhoods should have the same order.
%And finding an ordering tha
%Clusters may exist in muy
%Although \lining works best on 1 dimensional projections, clusters may exist in multiple dimensions. 
%
%There









