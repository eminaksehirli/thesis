\chapter{Conclusions}
\label{ch:conclusions}

%\chapterprecishere{
%In this chapter we formulate concluding remarks, summarise our main
%contributions, and provide possible directions for further research.
%\vspace{10em}
%}


An important leap on data utilisation changed the world forever.
Technical advancements and paradigm changes that led to a critical break point of how we use data,
can be referred by the umbrella term \emph{BigData}.
The world we are living in is now much more data-oriented
and considering its benefits and how it changes our lives, importance of data will not diminish soon.


\section{Main Contributions}

In this thesis, we investigate some of the challenges of the increasing the amount of data to analyse.
We propose new algorithms and provide software tools to better understand the data in a more efficient and effective manner.
%In a nutshell, we propose new algorithms for bridging two different areas of unsupervised data analysis,


In Chapter~\ref{ch:cartification}, we proposed \emph{cartification}, a novel method that transforms high dimensional relational databases into a transaction database.
Similarity measures and the traditional clustering methods that rely on them are disturbed in high-dimensional databases by the effects of so-called \emph{curse of dimensionality}.
Cartification transformation preserves the local neighborhoods and similarity information.
Therefore, the local cluster structures in the data are preserved and they form repetitive patterns in the transformed database.

Mining repetitive and frequent patterns in transaction databases is well-studied in the literature.
Our transformation allows the application of itemset mining techniques to mine patterns in relational databases.
We showed the applicability of this approach by proposing the \casclud method,
which finds subspace clusters in high-dimensional relational databases
using frequent itemset mining methods on the transformed databases.


In Chapter~\ref{ch:clon}, we investigated the special cases for cartification.
In particular, we studied the properties of neighborhoods in univariate projections.
We showed that when a total ordering of the data objects is possible,
the neighborhood databases have some intrinsic properties.
We proposed \clon, a method that exploits these properties to efficiently perform cartification transformation and find all of the clusters in the database.
With exhaustive experiments, we showed that \clon is orders of magnitude faster than the state-of-the-art methods including \casclud, while the quality of the detected clusters are better or comparable.
Experiments on a movie rating database showed that our method is a good fit for exploratory data analysis too.

Both the \casclud and \clon use binary transaction databases.
In Chapter~\ref{ch:carti-rank}, we experimented with a non-binary cartification transformation.
We convert high-dimensional relational databases into multiple rank matrices,
preserving the relative neighborhood information for the whole database.
Since objects in a cluster often co-occur in each other's neighborhoods,
\emph{minimal tiles} in the ranked matrices correspond to clusters in the data.
Exploiting the relative similarity of the objects in the neighborhoods provides robustness on parameter selection.
Moreover, the rank matrices of univariate projections have some intrinsic properties.
Using these properties, we developed \cartirank, an efficient algorithm that finds minimal tiles and, hence, the clusters.
We experimentally showed that the cluster finding capabilities of \cartirank is less dependent on the input parameters,
and therefore it can effectively find clusters of different sizes.
The high dimensional search strategy of \cartirank produces less redundant clusters than \casclud and \clon.


We gather up the ideas about the cartification and implemented \cartigui, a software tool for Visual Interactive Neighborhood Mining.
\cartigui can effectively visualize the neighborhoods, so that the cluster structures become visible.
A user can \emph{play} with the data by selecting interesting clusters, filtering out uninteresting objects, introducing new similarity measures, or applying different projections.
Unsupervised methods are also implemented in the application and the user can seamlessly switch between interactive analysis and unsupervised mining.

There are soft time constraints in interactive application settings.
Although \clon can improve the efficiency for some particular datasets, on other datasets we still have to employ frequent itemset mining methods.
However, FIM methods are computationally expensive and may overburden the client, failing to satisfy time constraints.
Fortunately, computationally expensive mining can be performed remotely and the resources of the client can be allocated for interactive analysis.
In chapter~\ref{ch:bigfim}, we investigate the applicability of frequent itemset mining methods on sate-of-the-art technologies.
We focus on Hadoop as it is one of the most widely-used platforms for distributed computing.
We studied the limitations of the platform along with the requirements of frequent itemset mining algorithms.
We found out that the overall performance depends on the balanced distribution of the work load over the computation nodes,
and proposed a heuristic to partition the prefix tree.
Our implementation performs much better that the publicly available implementations,
and it is itself publicly available under a free software license.



\section{Outlook}
Local neighborhoods hold essential similarity information.
In this thesis, we studied the local neighborhoods and possible applications that exploit their properties in the domain of pattern mining.
However, we barely scratched the surface of what is possible by exploiting the local neighborhoods.
Before looking forward, let us look on what we have already improved.

Three chapters of the thesis deal with the theoretical properties of local neighborhoods, or in other words, the \emph{cartification} transformation:
Chapter~\ref{ch:cartification} defines the general framework,
Chapter~\ref{ch:clon} improves the run time performance for continuous-value databases and
Chapter~\ref{ch:carti-rank} improves the accuracy by allocating more resources.
Both of the latter chapters investigate and improve the original method for some particular use cases.
Core features of these three methods are summarized in Table~\ref{tab:compare-methods}.

\begin{table}[tb]
	\centering
	\begin{tabular}{|p{2cm}|p{2.6cm}|p{2.6cm}|p{2.6cm}|}
		\hline
		& \casclud& \clon&\cartirank \\ \hline
		\hline
		Memory Requirement&$O(n \times d \times |C|)$ & $O(n \times d)$ & $O(n^2 \times d)$ \\ \hline
		Advantage&Can work with any kind of data&Fast&Improved accuracy \\ \hline
		Advantage&Noise aware&Noise aware&Less redundant output \\ \hline
		Limitation&Slow&Can only work with univariate data&Slow \\ \hline
		Limitation&Redundant clusters&Redundant clusters&High memory requirement \\ \hline


	\end{tabular}
	\caption{A short summary of the advantages and limitations of the methods. $n$ is the number of objects, $|C|$ is the cluster size, and $d$ is the number of attributes.}
	\label{tab:compare-methods}
\end{table}


One of the advantages of cartification is its usage of $k$-nearest neighborhoods.
When we disregard the actual distances and work with the relative similarities,
we avoid the pitfalls of different scales in the database.
On the other hand, $k$-nearest neighborhoods impose fixed-size neighborhoods, which have its own advantages and disadvantages.
On the plus side, $k$ parameter operates as a zooming factor, i.e., the user can select between large or small clusters by configuring the $k$ parameter.
This can be desired for the exploratory data analysis scenarios.
On the negative side, fixed-size neighborhoods can hinder the detection of clusters of different sizes.

\cartirank tackles the problem of fixed size neighborhoods by using rank matrices instead of binary neighborhoods.
However, not only storage requirements for rank matrices are high,
but also the tile mining is computationally more expensive than frequent itemset mining.
More balanced approaches that will relax the memory requirements should be possible and considering the quality improvement of \cartirank, worth researching.


Recurrency-based mining of our techniques are effective both at finding clusters and discarding noise.
However, computing the recurrencies is computationally expensive and
it is not efficient enough to depend on frequent itemset mining methods.
\casclud uses a sampling-based frequent itemset mining algorithm to improve efficiency and can still produce very good results since the FIM methods are known to produce redundant results.
\clon, on the other hand, focuses on continuous-value databases and exploit their intrinsic properties to speed-up the computation.
This shows us that studying the neighborhood databases can yield to discoveries of some other intrinsic properties that can be exploited to invent more efficient algorithms.

The cartification methods in this thesis focus on univariate projections of the data.
In Chapter~\ref{ch:clon} and Chapter~\ref{ch:carti-rank}, we showed that this focus improves the efficiency of the algorithms.
However, in some databases clusters are not separable on one-dimensional projections.
Combined with the limitations of fixed-size neighborhoods, mining only one-dimensional projections hinder the cluster detection on some real world datasets.
Of course, neighborhoods can still be computed and mined for redundancy in multi-dimensional projections,
but finding and selecting the projection dimensions is not trivial and, as a matter of fact, it is a well-known research problem.

Small frequent itemsets in a cartified database represent the micro-clusters in the original database.
By comparing micro-clusters of different projections, one can discover the relations between these projections.
Since the micro-clusters are localized, they can be more effective than feature selection techniques on high dimensional data.
We already provide a tool in \cartigui that uses them for finding relevant dimensions, cf.\ Chapter~\ref{ch:carti-gui}.
Further research on more sophisticated techniques can yield much more interesting results.


While the transformation emphasizes some information in the database, such as localities,
it disregards some other information, such as the actual distances.
By combining with recurrency-based mining methods, we can extract valuable knowledge from the transformed data.
In this spirit, it will be interesting to investigate the amount and the impact of the information loss, or gain in some cases, caused by the transformation.
Can it be measured theoretically or empirically?
How should we evaluate non-linear transformations in terms of information gain?
For example, non-binary transformation of neighborhoods, such as the one we have done in Chapter~\ref{ch:carti-rank},
improves the robustness of the methods.

We studied one example of non-binary neighborhood transformation, and shows that new approaches can address some issues.
Rank matrices for neighborhoods, and other approaches for neighborhoods transformation,
should be studied in more detail to find out whether they can address some other issues.
%is deemed to open new possibilities for data analysis.
As cartification put the frequent itemset mining algorithms at the disposal of relational databases,
non-binary transformation can be a bridge to the rich literature on mining ranked matrices.

Pixel-based representation of our GUI tool \cartigui provides a novel and intuitive way to get a better understanding of the data.
However, pixels are limited with the screen resolutions and therefore not suitable for large databases.
Considering the convenience of visual data mining, we should investigate more to discover new techniques that allows the representation and interactive analysis of larger databases.



Main motivations behind implementing the frequent itemset mining algorithms for a popular framework were to improve their accessibility and to evaluate their adaptivity to new architectures.
Since we released the code for the Hadoop framework, there have been many changes in the field:
Mahout dropped support for MapReduce and Apache Spark~\cite{web_spark} framework became a strong alternative for pure MapReduce implementations.
Applicability of our algorithms to the new platforms should be investigated.





%Memory cost: nkd, nd, n\^2d
%Main advantage: can work with any kind of data, fast, more robust
%disadvantage: slow, can only work on univariate data, memory requirement + slow
% : many clusters, many clusters, memory requirement

%\begin{itemize}
%	\item We propose \emph{cartification}, a novel transformation that transforms a high dimensional relational database to a transaction database.
%	\item We show that local neighborhoods can be exploited to tackle the curse of dimensionality.
%	\item We show that subspace clusters can be identified by running frequent pattern mining algorithms on neighborhood databases.
%	\item We study the properties of neighborhood databases of univariate attributes.
%	\item We propose an algorithm that exploits these properties to efficiently mine all of the subspace clusters.
%	\item We study the properties of non-binary neighborhood transformations.
%	\item We show that taking the order in the neighborhoods into account improves the robustness of cluster detection.
%	\item We provide a software tool that visualize the data with a uniform representation.
%	\item Our software tool seamlessly integrates interactive mining processes with unsupervised mining tools.
%	\item We study the usage of frequent itemset mining algorithms with state-of-the-art parallelization platforms.
%	\item We provide algorithms along with their implementations for frequent itemset mining on parallelization platforms.
%\end{itemize}

%\section{Future Work}
%
%Local neighborhoods hold essential similarity information.
%During our research on these neighborhoods, we discovered some of its intrinsic properties.
%In the thesis, we included applications and results of the properties that we can exploit.
%But we barely scratched the surface of what is possible by exploiting the local neighborhoods.
%
%
%One of the drawbacks of cartification is its use of computationally expensive frequent pattern mining methods, cf.\ Chapter~\ref{ch:cartification}.
%We investigated the properties of cartified databases of univariate attributes.
%This investigation resulted in an algorithm which is orders of magnitude faster than the original proposal.
%Investigation of non-univariate transformations can yield more insights
%which in turn can be used for more efficient and effective algorithms.
%
%Small frequent itemsets in a cartified database represent the micro clusters in the respective projection.
%By comparing micro-clusters of different projections, one can discover the relations between these projections.
%Since the micro-clusters are localized, they can be more effective than feature selection techniques on high dimensional data.
%We already provide a tool in \cartigui that uses them for finding relevant dimensions, cf.\ Chapter~\ref{ch:carti-gui}.
%Further research on more sophisticated techniques can yield much more interesting results.
%
%Non-binary transformation of neighborhoods, such as the one we have done in Chapter~\ref{ch:carti-rank}, is deemed to open new possibilities for data analysis.
%As cartification put the frequent itemset mining algorithms at the disposal of relational databases,
%non-binary transformation can be a bridge to the rich literature on mining ranked matrices.
%
%
%Main motivations behind implementing the frequent itemset mining algorithms for a popular framework were to improve their accessibility and to evaluate their adaptivity to new architectures.
%Since we released the code for the Hadoop framework, there have been many changes in the field:
%Mahout dropped support for MapReduce and Apache Spark~\cite{web_spark} framework became a strong alternative for pure MapReduce implementations.
%Applicability of our algorithms to the new platforms should be investigated.




