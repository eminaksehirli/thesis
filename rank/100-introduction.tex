\section{Introduction}
Clustering is one of the core techniques in data mining; however, one of the challenges when clustering high dimensional data is the \emph{curse of dimensionality}: when the number of dimensions is high, most points in the data become equidistant to each other, which makes it impossible to apply standard clustering techniques based on such distances.

One solution to this challenge is to perform \emph{subspace clustering}. In subspace clustering, we do not cluster the data points based on all attributes, but only cluster the points on a subset of the attributes. By looking for subspaces in which good clusters can be identified, subspace clustering allows for the discovery of useful clusters even in high dimensional data.

Many subspace clustering techniques have been proposed in the literature~\cite{cartification,SUBCLU,PROCLUS,STATPC}. However, many of these approaches require many parameters to be set, and furthermore can still be sensitive to the scale of the distance function that is used~\cite{mueller2009eval}. This makes it harder to get reliable results using these clustering techniques.

To address this challenge, the \emph{cartification} approach to subspace clustering was developed. For one attribute, the cartification approach studied in Chapter~\ref{ch:clon}~\cite{clon} proceeds as follows:
\begin{enumerate}
\item It calculates pairwise distances between all data points based on this attribute.
\item For each data point, it calculates the $k$ nearest data points based on these distances,
resulting in a neighborhood database in which the $i^{th}$ row contains a set of objects representing the $k$ points that are closest to the $i^{th}$ point.
This neighborhood database is a \emph{transaction database} and can be represented as a binary database in which each row has $k$ columns that are set to the value 1.
\item It searches for large sets of objects that are frequently repeated in the rows of the resulting binary matrix;
%	of 1s in the resulting binary matrix; 
each such set represents a set of data points that is close to each other, and hence represents a cluster for this attribute.
\end{enumerate}
For one attribute, the above algorithm returns a set of (possibly overlapping) clusters. By applying the approach recursively on the data points within each of these clusters, cartification finds subspaces consisting of sets of attributes and sets of data points.

An advantage of the cartification approach is that in step 2, it does not use the distance measure itself. This makes the approach less scale dependent; for instance, whether a logarithmic scale or a linear scale is used for an attribute has a smaller impact on the results.
Furthermore, infeasible computation of neighborhoods for each set of objects becomes unnecessary by computing the neighborhoods once and creating a neighborhood database.
%we only need to set two parameters: we need to set the parameter $k$ and an appropriate lower bound on the size of clusters that we wish to find.

Another advantage of cartification is its easy to determine parameters: neighborhood size $k$ and \emph{minimum cluster size}.
Setting $k$ and a lower bound on the size of clusters, both of which are functions of the expected cluster size,
is much easier than finding out the parameters such as density or distribution.
Our earlier work showed that after parameter tuning of all methods, cartification performed better than other subspace clustering approaches, in the sense that the clusters it found are better in terms of an F1 score~\cite{cartification,clon}.

On the other hand, the earlier work~\cite{clon} also showed that the quality of the results of cartification is highly dependent on the right choice for the parameter $k$.
Moreover, in some cases there is no right choice for $k$ because the dataset contains clusters of different sizes.
%while for the right choice of $k$, the clustering results are good, for a bad choice of $k$ the results are worse. 

In this chapter, we address this challenge of setting good robust parameters. We propose a new method that inherits many of the ideas of cartification, but is less sensitive to a good choice for the parameter $k$. Roughly speaking, this is the idea:
\begin{itemize}
\item instead of creating a binary transaction database in step 2 above, we create a new \emph{rank-based } matrix, in which for every data point $i$, we rank the other points according to the distance to the point $i$;
\item in this rank-based matrix, we apply a \emph{rank-based tiling} method to identify points that are all close to each other. This rank-based method has one other parameter $\theta$ that influences the size of the tiles that can be found.
\end{itemize}
Note that by creating a rank-based matrix, we still maintain the advantage of cartification that it does not rely on the scale of the original distance function; indeed, if we would threshold all the ranks at $k$, we could easily create the binary matrix that was used in the original cartification approach.

Furthermore, experimentally we will show that the proposed method is less sensitive to the choice of the parameter $\theta$ and obtains results that are equally well as the original cartification method, or better.

The outline of this chapter is as follows:
In Section~\ref{sec:rank-ordered}, we give an overview of cartification in our terms; in Section~III we introduce our proposed modification. 
We compare the binary cartification method and our algorithm in Section~\ref{sec:experiments},
and we conclude in Section~\ref{sec:conclude}.

