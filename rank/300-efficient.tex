\section{Mining Tiles Efficiently}
\label{sec:rank-mining}
In this section, we introduce our efficient algorithm that finds minimal tiles in an \emph{ordered ranked neighborhood matrix} efficiently
by exploiting its inherent properties that we introduced in Section~\ref{sec:rank-subspace}.

Since minimal tiles are always contiguous, cf.\ Property~\ref{pro:cont}, we only consider contiguous tiles.
As so, we reduce the search space from $O(2^{2n})$ to $O(n^{4})$.

A cluster is a set of objects that are similar to each other, thus
the objects in a cluster should be in each others close neighbors.
Note that, this is the core assumption of SNN clustering~\cite{SNNClustering} as well.
Therefore, we mine for square tiles, which represent mutually symmetric neighborhood relations.
This optimization further reduces the search space from $O(n^{4})$ to $O(n^{3})$.

As Properties \ref{pro:diagonal}, \ref{pro:row}, and \ref{pro:col} states, minimum values are always on and around the diagonal.
We exploit this property and ignore the tiles that do not include all of the possible diagonal values,
i.e., we only consider tiles that have two of their corners on the diagonals.
Therefore, we reduce the search space from $O(n^{3})$ to $O(n^{2})$.


Algorithm~\ref{algo:square} shows our tiling algorithm that finds the tile that have a minimum sum.
It takes the ordered rank matrix and number of objects as input parameters
and then computes the sum for all possible square tiles that have two corners on the diagonal to report the tile that has the minimum sum.
As a speed optimization step, it stops growing the tile if the new addition of values does not decrease the sum (lines 8 and 9).
This optimization exploits Properties~\ref{pro:row} and~\ref{pro:col}.



A minimal tile on an ordered ranked matrix represents a cluster structure in one of the dimensions.
After finding one dimensional clusters we further refine them by checking whether any of their subsets form clusters in any other dimensions.
Figure~\ref{fig:iterative} shows this process.
Objects that form a cluster in dimension 1 are selected in ordered ranked neighborhood matrix of dimension 2, $\mM^{A_{2}}$,
and then the tiling algorithm is run on this sub-matrix.
Any cluster that is found on this conditional matrix is a cluster in both dimension 1 and dimension 2.
Cluster search is extended to remaining dimensions in a similar fashion to find all combinations of dimensions that any subset of these objects form a cluster in.





\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=0.6\columnwidth,keepaspectratio=true,draft=false]{figs/iterative-tiling}
	\end{center}
	\caption{Iterative Tiling}
	\label{fig:iterative}
\end{figure}



%\textbf{Expand} tiling algorithm extends the tile by adding the column on the right or adding the row below as long as the tile sum decreases.
%It extends all the way in one direction and then switches to the other one.
%It keeps switching until extending neither of the directions decrease the tile sum.
%The process is shown in Algorithm~\ref{algo:expanding}.

\begin{algorithm}[tb]
	\begin{algorithmic}[1]
		\REQUIRE \mM: Rank Matrix - $\theta$, $n$: number of objects
		\ENSURE $min\_t$: Minimum tile

		\STATE $\textit{min\_t} := (-1,-1)$ \COMMENT{Minimum tile}
		\STATE $\textit{min\_ts} := INT\_MAX$ \COMMENT{Minimum tile sum}
		\FORALL{$s := \rightarrow n$}
			\STATE $ts := \mM_{ss}$ \COMMENT{Tile sum}
			\FORALL{$e := s+1 \rightarrow n$}
				\STATE $ns := \mM_{se} + \mM_{(s+1)e} + \cdots + \mM_{ee}$
				\STATE $ns := ns + \mM_{es} + \mM_{e(s+1)} + \cdots + \mM_{e(e-1)}$

				\IF{$ns > 0$}
					\STATE \textit{Continue with the next s}
				\ENDIF
				\STATE $ts := ts + ns$

				\IF{$ts < min\_ts$}
					\STATE $min\_ts := ts$
					\STATE $min\_t := (s,e)$
				\ENDIF
			\ENDFOR
		\ENDFOR
		\RETURN $min\_t$

	\end{algorithmic}
	\caption{\tiler: Tiling on Ordered Ranked Matrix}
	\label{algo:square}
\end{algorithm}

\begin{algorithm}[tb]
	\begin{algorithmic}[1]
		\REQUIRE \DB:~Database, $\theta$:~Approximate neighborhood size, $c$:~number of clusters, $m$:~number of attributes in \DB
		\ENSURE $C$: Subspace clusters

		\STATE $C := \emptyset$
		\STATE $\mu = \theta/4$ \COMMENT{Minimum tile size}
		\STATE initialize empty stack $S$
		\STATE Create sorted rank neigh. matrices $\mM^{A_{1}}$ through $\mM^{A_{m}}$
		\FORALL{$d := 1 \leftarrow m$}
			\FOR{$0 \rightarrow c$}
				\STATE $t := \tiler(\mM^{A_{d}})$
				\IF{$|t| > \mu$}
					%\STATE $C^{A_{d}} := C^{A_{d}} \cup t)$
					\STATE $C := C \cup (t, \{A_{d}\})$
					\STATE push $(t, \{A_{d}\}, \Ats - \{A_{d}\})$ to $\mathcal{S}$
				\ENDIF
			\ENDFOR
		\ENDFOR

		\REPEAT
		\STATE $(t, D, P) \assign$ pop $\mathcal{S}$
		\FORALL{$p \in P$}
			\STATE $\mM' :=$ get sub-matrix of $\mM^{A_p}$ for objects in $t$
			\FOR{$0 \rightarrow c$}
				\STATE $t' := \tiler(\mM', |t|)$
				\IF{$|t| > \mu$}
					\STATE $C := C \cup (t', D \cup \{p\})$
					\STATE push $(t', D \cup \{p\}, P - \{p\})$ to $\mathcal{S}$
				\ENDIF
			\ENDFOR
		\ENDFOR
		\UNTIL{$\mathcal{S}$ is empty}

	\end{algorithmic}
	\caption{\cartirank: Find subspace clusters}
	\label{algo:cartirank}
\end{algorithm}

%\begin{algorithm}[tb]
%	\begin{algorithmic}[1]
%		\REQUIRE M: Rank Matrix - $\theta$, $n$: number of objects
%		\ENSURE $T$: Minimum $k$ tiles
%
%		\STATE \COMMENT{sr: Starting Row, sc: Starting Column, er: End Row, ec: End Column}
%		\FORALL{$sr := 0 \rightarrow n$}
%			\FORALL{$sc := 0 \rightarrow n$}
%				\WHILE{$M_{sr,sc} > 0$ \AND $sc<sr$}
%					\STATE sc++ \COMMENT{Expand only from non-positive values}
%				\ENDWHILE
%				\IF{$M_{sr,sc} > 0$}
%					\STATE continue $sr$ \COMMENT{Do not expand from upper triangle}
%				\ENDIF
%				\STATE $er := sr, ec := sc$
%				\STATE expandLeft := \TRUE, expandDown := \TRUE
%				\WHILE{expandDown \OR expandRight}
%					\WHILE{expandRight}
%						\IF{$t(sr,sc,er,ec) > t(sr,sc,er,ec-1)$ \AND $ec >= er$}
%							\STATE extendRight := \FALSE
%							\STATE break
%						\ENDIF
%						\STATE ec++, extendDown := \TRUE
%						\STATE $T := T \cup t(sr,sc,er,ec)$
%					\ENDWHILE
%					\WHILE{expandDown}
%						\IF{$t(sr,sc,er,ec) > t(sr,sc,er-1,ec)$ \AND $ec <= er$}
%							\STATE extendDown := \FALSE
%							\STATE break
%						\ENDIF
%						\STATE er++, extendRight := \TRUE
%						\STATE $T := T \cup t(sr,sc,er,ec)$
%					\ENDWHILE
%				\ENDWHILE
%			\ENDFOR
%		\ENDFOR
%	\end{algorithmic}
%	\caption{Expanding Tiling Algorithm}
%	\label{algo:expanding}
%\end{algorithm}
