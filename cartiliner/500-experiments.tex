\section{Empirical Evaluation\label{sec:liner:experiments}}
\subsection{Experimental Setup}
We evaluate our method on heterogeneous datasets, i.e., datasets with different scales, in terms of both dimension scale and cluster distribution.
We also conduct a set of experiments to evaluate the scalability of our method according to the number of objects, number of dimensions and the amount of noise.
To see whether our method is a good fit for real world scenarios, we conduct experiments on very high dimensional real world datasets.
We compare the quality of the clusters that are found by our method with the state of the art subspace clustering methods, such as CartiClus~\cite{cartification}, FIRES~\cite{FIRES}, PROCLUS~\cite{PROCLUS}, STATPC~\cite{STATPC}, and SUBCLU~\cite{SUBCLU}, cf.\ Section~\ref{sec:related}.

Object cluster finding capabilities of the methods are evaluated by the supervised F1 Measure.
The F1 score is the harmonic mean of \textit{precision} and \textit{recall}.
\textit{precision} between a found cluster $C_{i}$ and a true cluster $\mathbf{C}_{j}$ is ${|C_{i} \cap \mathbf{C}_{j}|}\over|C_{i}|$,
and the \textit{recall} is ${|C_{i} \cap \mathbf{C}_{j}|}\over|\mathbf{C}_{j}|$.
Following the practice in literature~\cite{STATPC,mueller2009eval}, we match found clusters to true clusters based on their intersection.
A detailed explanation of the F1 Score is given in Section~\ref{section:experiments}.
We report both the \emph{F1-Precision} (\ref{eqn:f1-p}) and \emph{F1-Recall} (\ref{eqn:f1-r})for better comparison and fairness.
Where appropriate, we evaluate the subspace discovery capabilities by using the established E4SC score~\cite{mueller2009eval}, cf.\ Section~\ref{section:experiments}.

Runtime results are given for the performance versus scale experiments.
For CartiClus, we use the implementation from the authors~\cite{cartification} and for the other methods we use the OpenSubspace package~\cite{mueller2009eval}.
Our method is implemented in Java. All of the experiments are run on a computer with Intel i7 CPU and 8 GB of memory running with a GNU/Linux operating system.
For the reproducibility of the experiments, a cross-platform implementation of the method and the information about the datasets are available on our web page.\!\footnote{\url{http://adrem.uantwerpen.be/clon}}


%\textcolor{blue}{Comparison between our method and other competitors is supplied for the real world data and the datasets that we generated.
%For the performance of the competitors on other datasets, readers are kindly referred to the original papers that the data sets are published.
%}


\subsection{Heterogeneous Datasets}
We measure the cluster discovery capabilities of our method on datasets that (1) have different scaling in different dimensions and (2) have clusters with different spreads.
To evaluate these capabilities we generated two sets of datasets:
(1) After creating a dataset with 8 clusters that are hidden in a total of 12 dimensions, half of these dimensions are scaled with factors of 2, 4, 8, 16, 32, 100, and 200.
Properties of these datasets are shown in Table~\ref{tab:data-clon-ds}.
(2) Two equivalent clustering formations that have 4 clusters hidden in 6 dimensions are created. One of these datasets is scaled with factors of 2, 4, 8, 16, 32, 100, and 200, then merged with the other one along with some uniformly random dimensions.
Properties of these datasets are shown in Table~\ref{tab:data-clon-cs}.

\begin{table}
	\centering
	\small
	\begin{tabular}{|c|>{\centering\arraybackslash}p{1.1cm}|>{\centering\arraybackslash}p{1.8cm}|>{\centering\arraybackslash}p{1.1cm}|>{\centering\arraybackslash}p{1.2cm}|>{\centering\arraybackslash}p{1.6cm}|>{\centering\arraybackslash}p{0.9cm}|}
		\hline
		&\textbf{\# of Objects}&\textbf{\# of Dimensions}&\textbf{Cluster Size}&\textbf{Dims per Cluster}&\textbf{Max/Min Dimension Range}&\textbf{Noise Ratio} \\ \hline \hline
		DS-1&840&12&100&4--5&1&5\% \\ \hline
		DS-2&840&12&100&4--5&2&5\% \\ \hline
		DS-4&840&12&100&4--5&4&5\% \\ \hline
		DS-8&840&12&100&4--5&8&5\% \\ \hline
		DS-16&840&12&100&4--5&16&5\% \\ \hline
		DS-32&840&12&100&4--5&32&5\% \\ \hline
		DS-100&840&12&100&4--5&100&5\% \\ \hline
		DS-200&840&12&100&4--5&200&5\% \\ \hline
	\end{tabular}
	\caption{Properties of the datasets with different scaling per dimension. For example, ranges of all of the dimensions of DS-1 are [0,500] while for DS-8, some of the ranges are [0,500] and some of them are [0,4000].}
	\label{tab:data-clon-ds}
\end{table}

\begin{table}
	\centering
	\small
	\begin{tabular}{|c|>{\centering\arraybackslash}p{1.1cm}|>{\centering\arraybackslash}p{1.8cm}|>{\centering\arraybackslash}p{1.1cm}|>{\centering\arraybackslash}p{1.2cm}|>{\centering\arraybackslash}p{1.6cm}|>{\centering\arraybackslash}p{0.9cm}|}
		\hline
		&\textbf{\# of Objects}&\textbf{\# of Dimensions}&\textbf{Cluster Size}&\textbf{Dims per Cluster}&\textbf{Max/Min Cluster Radius}&\textbf{Noise Ratio} \\ \hline \hline
		CS-1&840&9&100&2--3&1&5\% \\ \hline
		CS-2&840&9&100&2--3&2&5\% \\ \hline
		CS-4&840&9&100&2--3&4&5\% \\ \hline
		CS-8&840&9&100&2--3&8&5\% \\ \hline
		CS-16&840&9&100&2--3&16&5\% \\ \hline
		CS-32&840&9&100&2--3&32&5\% \\ \hline
		CS-100&840&9&100&2--3&100&5\% \\ \hline
		CS-200&840&9&100&2--3&200&5\% \\ \hline
	\end{tabular}
	\caption{Properties of the datasets with different cluster spreads. For example, radiuses of the clusters in CS-1 are all approximately 10 units while in CS-8, radiuses of some of the clusters are 10 and some of them are 80 units.}
	\label{tab:data-clon-cs}
\end{table}

Figures~\ref{fig:chart-dim-scale-f1r} and~\ref{fig:chart-dim-scale-f1p} respectively show the \emph{F1-Recall} and \emph{F1-Precision} scores of the methods on dimension scaling datasets.
And Figures~\ref{fig:chart-cluster-scale-f1r} and~\ref{fig:chart-cluster-scale-f1r} respectively show \emph{F1-Recall} and \emph{F1-Precision} scores of the methods on cluster scaling datasets.
On both set of experiments, \clon produces very stable and high quality clusters.
CartiClus is comparable with \clon, which shows the effectiveness of nearest-neighborhood-based clustering.
Radius-based neighborhood evaluation of FIRES and distance sensitive projections of PROCLUS cannot cope with scaling.
STATPC produces high quality, although unstable, results because of its thorough search, which becomes a burden in terms of execution time, cf.\ Section~\ref{sec:experiments-scala}.
The results of SUBCLU are interesting: while \emph{F1-Recall} results are very high, \emph{F1-Precision} results are very low.
This is because SUBCLU detects many clusters and although the known clusters are among them the majority of them are not in accordance with the known clusters.


Figure~\ref{fig:chart-dim-scale-e4} and Figure~\ref{fig:chart-cluster-scale-e4} show the E4SC scores for the same sets of datasets.
While some of the methods perform poorly on dimension detection, for most of the methods, dimension finding capabilities of the methods are in parallel with the object cluster finding capabilities.

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_f1r}
	\end{center}
	\caption{F1-Recall scores of the methods on datasets with dimension of various scales}
	\label{fig:chart-dim-scale-f1r}
\end{figure}
\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_f1p}
	\end{center}
	\caption{F1-Precision scores of the methods on datasets with dimension of various scales}
	\label{fig:chart-dim-scale-f1p}
\end{figure}
\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_e4sc}
	\end{center}
	\caption{E4SC scores of the methods on datasets with dimension of various scales}
	\label{fig:chart-dim-scale-e4}
\end{figure}


\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_clusterScale_e4sc}
	\end{center}
	\caption{E4SC scores of methods on datasets that have clusters of various scales}
	\label{fig:chart-cluster-scale-e4}
\end{figure}
\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_clusterScale_f1r}
	\end{center}
	\caption{F1-Recall scores of methods on datasets that have clusters of various scales}
	\label{fig:chart-cluster-scale-f1r}
\end{figure}
\begin{figure}[t]
	\begin{center}
		\includegraphics[width=.9\columnwidth,keepaspectratio=true,draft=false]{figs/chart_clusterScale_f1p}
	\end{center}
	\caption{F1-Precision scores of methods on datasets that have clusters of various scales}
	\label{fig:chart-cluster-scale-f1p}
\end{figure}


\subsection{Scalability Results}
\label{sec:experiments-scala}
We conduct experiments to understand the scalability of our algorithm according to the data size, dimensionality, and the noise ratio.
For these experiments we use the datasets that are used in the literature for similar comparative studies~\cite{mueller2009eval,cartification}.
Properties of the datasets are given in Table~\ref{tab:data-clon-scale}.

\begin{table}
	\centering
	\begin{tabular}{|c|c|>{\centering\arraybackslash}p{1.9cm}|c|>{\centering\arraybackslash}p{1.3cm}|>{\centering\arraybackslash}p{1cm}|}
		\hline
		\textbf{Name}&\textbf{\# of Objects}&\textbf{\# of Dimensions}&\textbf{Cluster Size}&\textbf{Dims per Cluster}&\textbf{Noise Ratio} \\ \hline \hline
		S1500&\textbf{1595}&20&\textbf{~150}&12--16&~8.5\% \\ \hline
		S2500&\textbf{2658}&20&\textbf{~250}&12--16&~8.5\% \\ \hline
		S3500&\textbf{3722}&20&\textbf{~350}&12--16&~8.5\% \\ \hline
		S4500&\textbf{4785}&20&\textbf{~450}&12--16&~8.5\% \\ \hline
		S5500&\textbf{5848}&20&\textbf{~550}&12--16&~8.5\% \\ \hline

		D05&1595&\textbf{5}&~150&3--4&~9\% \\ \hline
		D10&1595&\textbf{10}&~150&5--8&~9\% \\ \hline
		D15&1595&\textbf{15}&~150&9--12&~9\% \\ \hline
		D25&1595&\textbf{25}&~150&15--20&~9\% \\ \hline
		D50&1595&\textbf{50}&~150&30--40&~9\% \\ \hline
		D75&1596&\textbf{75}&~150&45--61&~9\% \\ \hline

		N10&\textbf{1611}&20&~150&12--16&\textbf{~10\%} \\ \hline
		N30&\textbf{2071}&20&~150&12--16&\textbf{~30\%} \\ \hline
		N50&\textbf{2900}&20&~150&12--16&\textbf{~50\%} \\ \hline
		N70&\textbf{4833}&20&~150&12--16&\textbf{~70\%} \\ \hline

		ND10&1050&\textbf{20}&~100&3--6&~5\% \\ \hline
		ND50&1050&\textbf{60}&~100&4--7&~5\% \\ \hline
		ND100&1050&\textbf{110}&~100&4--6&~5\% \\ \hline
		ND200&1050&\textbf{210}&~100&4--6&~5\% \\ \hline
	\end{tabular}
	\caption{Properties of the datasets for scalability experiments}
	\label{tab:data-clon-scale}
\end{table}

%Figure~\ref{fig:chart-size-scale} shows the quality and run time for the datasets with an increasing number of objects.
Figure~\ref{fig:chart-size-scale} shows the run times of the methods on datasets with an increasing number of objects.
This set of datasets include 5 different datasets which have approximately 1500, 2500, 3500, 4500, and 5500 objects and 20 dimensions.
Our method scales well compared to the algorithms that search the combinations of dimensions.

Figure~\ref{fig:chart-dim-scale} shows the run times for different methods on datasets with an increasing number of dimensions.
We conduct a test on 6 different datasets that have approximately 1500 objects in 5, 10, 15, 25, 50, and 75 dimensions.
Our method scales well with the increasing number of dimensions while utilizing the information in combinations of dimensions.
Note that the missing values indicates that the method did not complete in a practical time or failed because of excessive memory requirements.

F1 scores (\ref{eqn:f1-set}) for the experiments on scalability w.r.t.\ size and dimension are shown in Figures~\ref{fig:chart-size-scale-q} and~\ref{fig:chart-dim-scale-q}.
%Results clearly show that the quality of the clusterings is not affected by the number of dimensions.
%General trend of running time is a quadratic function of the number of dimensions.
%However, the time result for dataset with 75 dimensions show that depending on the characteristics of the dataset, execution can be faster because of a higher level of pruning.



\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=.8\columnwidth,keepaspectratio=true,draft=false]{figs/chart_sizeScale_q}
	\end{center}
	\caption{F1 Scores for the datasets with increasing number of objects}
	\label{fig:chart-size-scale-q}
\end{figure}

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=.8\columnwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_q}
	\end{center}
	\caption{F1 Scores for the datasets with increasing number of dimensions}
	\label{fig:chart-dim-scale-q}
\end{figure}

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_sizeScale}
	\end{center}
	\caption{Execution times for datasets with increasing number of objects}
	\label{fig:chart-size-scale}
\end{figure}
\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale}
	\end{center}
	\caption{Execution times for datasets with increasing number or dimensions}
	\label{fig:chart-dim-scale}
\end{figure}

Figures~\ref{fig:noise-scale-f1r} and~\ref{fig:noise-scale-f1p} show the quality results for the datasets that contain 10\%, 30\%, 50\%, and 70\% noise.
Our method and CartiClus can effectively discard noise and find the known clusters in noisy databases thanks to their recurrency and neighborhood-based foundations.
In Figure~\ref{fig:noise-scale-f1p}, we see that \clon detects some cluster that are not in the known set of clusters.
This is because \clon finds all of the clusters in all of the dimensions and it is possible that some of these noise objects form clusters.
Overall, \clon is one of the best performers in noisy data along with \casclud.

Figures~\ref{fig:noisedim-scale-f1r} and~\ref{fig:noisedim-scale-f1r} show the capability of irrelevant dimension detection.
A database which has 10 clusters hidden in subsets of 10 dimension is created. Then, 10, 50, 100 and 200 uniformly randomly generated dimensions are added to the dataset.
Here again, we see that \clon, \casclud and SUBCLU can effectively find the known clusters.
However, only \clon, \casclud, and STATPC can discard irrelevant dimensions and report \emph{only} the known clusters.
STATPC and SUBCLU did not complete in meaningful time for some of the datasets, hence the missing values.


\begin{figure}[tb]
	\begin{center}
				\includegraphics[width=0.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_noiseScale_f1r}
	\end{center}
	\caption{F1-Recall scores for datasets with increasing number of noise objects}
	\label{fig:noise-scale-f1r}
\end{figure}
\begin{figure}[tb]
	\begin{center}
				\includegraphics[width=0.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_noiseScale_f1p}
	\end{center}
	\caption{F1-Precision scores for datasets with increasing the number of noise objects}
	\label{fig:noise-scale-f1p}
\end{figure}

\begin{figure}[tb]
	\begin{center}
				\includegraphics[width=0.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_noiseDimScale_f1r}
	\end{center}
	\caption{F1-Recall scores for datasets with increasing number of irrelevant dimensions}
	\label{fig:noisedim-scale-f1r}
\end{figure}
\begin{figure}[tb]
	\begin{center}
				\includegraphics[width=0.95\columnwidth,keepaspectratio=true,draft=false]{figs/chart_noiseDimScale_f1p}
	\end{center}
	\caption{F1-Precision scores for datasets with increasing number of irrelevant dimensions}
	\label{fig:noisedim-scale-f1p}
\end{figure}

%For all of the datasets, \clon can detect the relevant dimensions with a high accuracy.
%Although we did not explicitly report, our method did not report any wrong dimension information in any of the experiments.

\subsection{Real World Datasets}
To evaluate our method, we used two gene expression datasets, \textit{Nutt} and \textit{Alon}, along with a movie rating dataset, \textit{movies}.
\textit{Alon} is a dataset of 2000 gene expression across 62 tissues from a colon cancer research.
The tissues are grouped into 2 categories: 40 healthy tissues and 22 tissues with tumor~\cite{alon}.
\textit{Nutt} dataset contains expressions of 1377 genes on 50 glioma tissue samples that are grouped into 4 different pathology categories~\cite{nutt}.
High dimensional nature of both datasets make the clusteringchallenging even for subspace clustering methods.
Table~\ref{tab:genes} shows the F1 scores of the methods on these data sets.
``n/a'' indicates that the method did not complete in a practical time or failed because of excessive memory requirements.
These results show that, although our method searches the clusters in combinations of dimensions, it keeps being scalable even for very high dimensional datasets.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|} \hline
		&\textit{Alon}&\textit{Nutt}\\ \hline
		Our method&\textbf{0.78}&\textbf{0.75}\\ \hline
		PROCLUS&0.46&0.44\\ \hline
		FIRES&0.52&0.55\\ \hline
		SUBCLU&0.58&n/a\\ \hline
		STATPC&n/a&n/a\\ \hline
		CartiClus&n/a&n/a\\ \hline
	\end{tabular}
	\caption{F1 scores for gene expression datasets}
	\label{tab:genes}
\end{table}

We conduct an exploratory data analysis on a movie ratings dataset from GroupLens.\!\footnote{\url{http://grouplens.org/}}
We use 10M movielens dataset which includes 10000054 ratings applied to 10681 movies by 71567 users from the website \url{http://movielens.org}.
We use movies as objects and users as attributes.
We start with finding the most similar 3 movies according to the user ratings.
The result is, not surprisingly, the original Star Wars trilogy.
When we lower the similarity threshold, we start to see a cluster of the Lord of the Rings Trilogy along with some other high profile movies
in clusters of science fiction movies, action movies and crime movies.
Some of the selected clusters are given in Table~\ref{tab:movies-3}.

We continue our analysis by increasing the number of similar movies to 6.
Considering the similarity of the original Star Wars trilogy, we search for a cluster of all 6 of the released Star Wars movies, with no luck.
Actually, lack of this 6 pack of Star Wars movies cluster does not contradict with the general opinion of the fans of the franchise because the last 3 movies are not as popular as the originals.
Table~\ref{tab:movies-6} shows some of the interesting clusters of size 6,
which includes a cluster of two most popular trilogies, 
a cluster of distopian movies, a cluster of popular thrillers, and a cluster of very popular classic movies.


\begin{figure}[tb]
	\begin{center}
		\subfloat[Clusters of size 3\label{tab:movies-3}]{
			\begin{tabular}{|l|} \hline
			 Star Wars: A New Hope (1977) \\
			 Star Wars: The Empire Strikes Back (1980) \\
			 Star Wars: Return of the Jedi (1983) \\ \hline
			 LotR: The Fellowship of the Ring, The (2001)\\
			 LotR: The Two Towers, The (2002) \\
			 LotR: The Return of the King, The (2003) \\ \hline
			 Back to the Future (1985) \\
			 Terminator, The (1984) \\
			 Terminator 2: Judgment Day (1991)\\ \hline
			 Die Hard (1988) \\
			 Terminator, The (1984) \\
			 Terminator 2: Judgment Day (1991)\\ \hline
			 Usual Suspects, The (1995) \\
			 Pulp Fiction (1994) \\
			 Silence of the Lambs, The (1991)\\ \hline
			\end{tabular}
		}
		\subfloat[Clusters of size 6\label{tab:movies-6}]{
			\begin{tabular}{|l|} \hline
				Star Wars: A New Hope (1977) \\
				Star Wars: The Empire Strikes Back (1980) \\
				Star Wars: Return of the Jedi (1983) \\ 
				LotR: The Fellowship of the Ring, The (2001)\\
				LotR: The Two Towers, The (2002) \\
				LotR: The Return of the King, The (2003) \\ \hline
				Brazil (1985) \\
				Dr. Strangelove (1964) \\
				Clockwork Orange, A (1971) \\
				2001: A Space Odyssey (1968) \\
				Blade Runner (1982) \\
				Alien (1979) \\ \hline
				Chinatown (1974) \\
				Rear Window (1954) \\
				North by Northwest (1959) \\
				Vertigo (1958) \\
				Psycho (1960) \\
				Silence of the Lambs, The (1991) \\ \hline
				Third Man, The (1949) \\
				Citizen Kane (1941) \\
				Godfather: Part II, The (1974) \\
				Chinatown (1974) \\
				Godfather, The (1972) \\
				Taxi Driver (1976) \\ \hline
			\end{tabular}
		}
	\end{center}
	\caption{Clusters of Movies}
	\label{fig:movies}
\end{figure}
