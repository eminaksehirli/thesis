\section{Experiments}\label{section:experiments}

In this section we discuss empirical results and demonstrate the capabilities of cartification for subspace cluster detection.

\textbf{Competitors:}
We compare against five well-known clustering algorithms: (1) \emph{Proclus}~\cite{PROCLUS} is a projected clustering algorithm. According to the recent evaluation study on subspace clustering~\cite{mueller2009eval}, it can be considered to be one of the best performing subspace clustering approaches. 
(2) \emph{K-Means}~\cite{kmeans}
over all dimensions (full space), (3) \emph{RP-KM} is K-Means on a set of random projections (RP), (4) \emph{PCA-KM} is K-Means on a transformed data space by Principal Component Analysis (PCA), and
(5) \emph{CSPA}~\cite{strehl2002cluster} is an ensemble clustering algorithm. 
We use 10 runs of K-Means on separate dimensions to produce input clusterings and combine them with CSPA.
To improve the stability of the clusters, we use k-means++~\cite{arthur2007k} instead of the original K-Means.

\textbf{Parameters:}
Since each of the methods we consider are stochastic, we run each setup for 10 times and report the average results.
For all competitors, we try to optimize the respective parameter settings. For example, we use the number of expected dimensions per cluster as input for Proclus and RP-KM. In case of K-Means we provide the true number of clusters as input parameter. 
As default parameter setting of our approach, we set \minlen parameter to half of to the \emph{expected cluster size} parameter. Since it is just a minimum, this setting increased the robustness of the algorithm without limiting the ability of finding the whole clusters. The parameter $n$ is set to 100 for all of the experiments.

\textbf{Experiment Setup:}
We explore the influence of dimensionality on performance, noise awareness and relevant subspace detection capabilities of the algorithms. We use both synthetically generated data and publicly available real world benchmark data, as also used in a recent evaluation study~\cite{mueller2009eval}.
Properties of the databases are shown in Table~\ref{tab:data-cart}.
All of these databases are provided online,\!\footnote{http://adrem.uantwerpen.be/cartification} together with all algorithms, which are implemented in Java.
Each run of each algorithm was completed under 2 minutes on a standard PC with Intel Core-i7 processor and 8 GB of memory running GNU/Linux. 
Note that, since the cartification transformation has to be done only once we run \casclud on cartified data. 


\textbf{Evaluation Measures:}
For quality assessment, we use the ground truth of the synthetic data and class labels of the real world data. To compare fairly with other methods, we use two well-established quality measures: \emph{F1} and \emph{E4SC}, which have also been used in a variety of subspace cluster publications~\cite{P3C,STATPC,RESCU,mueller2009eval,gunnemann2011external}.
\emph{F1} measures the harmonic mean of \emph{precision} and \emph{recall} between a set of hidden clusters and the set of detected clusters. For a given cluster $C$ and a detected cluster $C'$, \emph{precision} is the ratio of common objects between $C$ and $C'$ to the size of $C'$, while \emph{recall} is the ratio of the size of the intersection to the size of $C$.
Formally,
\begin{align*}
	\textit{precision}(C,C') &= \frac{|C \cap C'|}{|C'|} \\
	\textit{recall}(C,C') &= \frac{|C \cap C'|}{|C\,|}
\end{align*}
\begin{equation}
	\textit{F1}(C,C') = \frac{2 \times \textit{recall}(C,C') \times \textit{precision}(C,C')}{\textit{recall}(C,C') + \textit{precision}(C,C')}
	%= \frac{2 \times |C \cap C'|}{|C\,| + |C'|}
	\label{eqn:f1-cluster}
\end{equation}

Since the clusters has no labels, unlike a classification problem, we have to match the detected clusters to the given clusters.
We follow the common practice in the literature and match the clusters that produce the highest F1 score.
To keep the assessments fair and accurate, we use two intermediate measures:
We first match every found cluster to a known cluster, compute their pairwise F1 scores, and then, take the average of these F1 scores.
We call this measure \emph{F1-Precision}, as it measures how \emph{accurate} the detected clusters are.
Similarly, we use \emph{F1-Recall} measure. It matches every known cluster to a found clusters and take the average of the pairwise F1 scores.
And lastly, we report the harmonic mean of \emph{F1-Precision} and \emph{F1-Recall} scores as the F1 score.
Let $\mathbf{C}$ and $\mathbf{C'}$ respectively be a set of given clusters and a set of detected clusters, we compute the F1 scores as follows:

\begin{align}
	\textit{F1-Precision}(\mathbf{C}, \mathbf{C'}) &= \frac{1}{|\mathbf{C}'|}\times\sum_{C' \in\, \mathbf{C'}}\max(\textit{F1}(C,C')) \label{eqn:f1-p}\\
	\textit{F1-Recall}(\mathbf{C}, \mathbf{C'}) &= \frac{1}{|\mathbf{C}\,|}\times \sum_{C\, \in\, \mathbf{C}\,}\max(\textit{F1}(C,C')) \label{eqn:f1-r}
\end{align}
\begin{equation}
	\textit{F1}(\mathbf{C}', \mathbf{C}) = \frac{2 \times \textit{F1-Recall}(\mathbf{C},\mathbf{C}') \times \textit{F1-Precision}(\mathbf{C},\mathbf{C}')}{\textit{F1-Recall}(\mathbf{C},\mathbf{C}') + \textit{F1-Precision}(\mathbf{C},\mathbf{C}')}
	\label{eqn:f1-set}
\end{equation}


In addition to \textit{F1}, which only measures the object-wise quality of a cluster, we use \textit{E4SC} to assess the quality of detected subspaces as well as the quality of the detected objects.
\emph{E4SC} measures the subspace-aware overlap between clusters by applying the F1 score on the attribute level~\cite{gunnemann2011external}.
\emph{E4SC} between two subspace clusters is computed as follows:
\begin{enumerate}
	\item Clusters are extended by finding the Cartesian product of objects and attributes.
		For a subspace cluster $SC = (C, S)$, extended subspace cluster is $e(SC) = C \times S$.
	\item Each object in each attribute is treated as a new \emph{micro}-object.
	\item F1 score is computed between these extended clusters using the \emph{micro}-objects.
\end{enumerate}
Formally, \emph{E4SC} between two subspace clusters $SC$ and $SC'$ is
\[
\textit{E4SC}(SC, SC') = \textit{F1}(e(SC), e(SC'))
\]
For example let two subspace clusters be
$SC_1 = (\{\vo_{1}, \vo_{2}, \vo_{3}, \vo_{4}, \vo_{5}\}, \{A_1, A_2, A_3, A_4\})$ and
$SC_2 = (\{\vo_{3}, \vo_{4}, \vo_{5}, \vo_{6}\}, \{A_2, A_3, A_4, A_5\})$.
As shown in Figure~\ref{fig:e4sc}; when we extend the cluster $SC_1$, we have
\[
e(SC_1) = \{\vo_{1}^{A_1}, \vo_{2}^{A_1}, \vo_{3}^{A_1}, \vo_{4}^{A_1}, \vo_{5}^{A_1}, \vo_{1}^{A_2},\cdots,\vo_{5}^{A_2},\vo_{1}^{A_3},\cdots,\vo_{5}^{A_5}\}
\]
\emph{E4SC} between $SC_1$ and $SC_2$ is
\begin{align*}
\textit{E4SC}(SC_1,SC_2) &= \textit{F1}(e(SC_1),ec(SC_2)) \\
&= 2 \times \frac{|e(SC_1) \cap e(SC_2)|}{|e(SC_1)| + |e(SC_2)|}\\
&= 2 \times \frac{3 \times 3}{5 \times 4 + 4 \times 4} 
= 2 \times \frac{9}{20+16} = 0.5
\end{align*}

Following the computation in~(\ref{eqn:f1-set}), \emph{E4SC} score between two \emph{sets} of clusters is computed as the \textit{F1} score between the sets of extended clusters:
\[
\textit{E4SC}(\mathbf{SC}, \mathbf{SC}') = \textit{F1}(e(\mathbf{SC}), e(\mathbf{SC}'))
\]



\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.6\textwidth,keepaspectratio=true,draft=false]{figs/e4sc}
	\end{center}
	\caption{E4SC between two subspace clusters is computed by measuring the overlap of both objects and dimensions.}
	\label{fig:e4sc}
\end{figure}

%Due to space limitations, however, we refer the interested reader to the article in which \textit{E4SC} was proposed~\cite{gunnemann2011external}. 





\begin{table}
	\centering
	\begin{tabular}{|c|c|>{\centering\arraybackslash}p{1.9cm}|c|>{\centering\arraybackslash}p{1.3cm}|>{\centering\arraybackslash}p{1cm}|}
		\hline
		&\textbf{\# of Objects}&\textbf{\# of Dimensions}&\textbf{Cluster Size}&\textbf{Dims per Cluster}&\textbf{Noise Ratio} \\ \hline \hline
		D10&1595&10&~150&5--8&~9\% \\ \hline
		D25&1595&25&~150&15--20&~9\% \\ \hline
		D75&1596&75&~150&45--61&~9\% \\ \hline
		N30&2071&20&~150&12--16&~30\% \\ \hline
		N50&2900&20&~150&12--16&~50\% \\ \hline
		N70&4833&20&~150&12--16&~70\% \\ \hline
		ND10&1050&20&~100&3--6&~5\% \\ \hline
		ND50&1050&60&~100&4--7&~5\% \\ \hline
		ND200&1050&210&~100&4--6&~5\% \\ \hline
		Vowel&990&11&99&11&0 \\ \hline
		Shape&160&17&11--20&17&0 \\ \hline
		Alon&62&2000&22--40&?&0 \\ \hline
	\end{tabular}
	\caption{Properties of the datasets}
	\label{tab:data-cart}
\end{table}

\subsection{Dimensionality}
In order to assess the subspace cluster detection capabilities of the methods w.r.t.\ increasing number of dimensions, we conduct experiments on datasets where the number of objects and the average ratio of relevant dimensions per cluster stay constant while the number of dimensions increases.
Datasets D10, D25 and D75 have 10, 25 and 75 dimensions, respectively.%~\cite{muller:09:sscl}.

The results for these experiments are shown in Figures~\ref{fig:carti-dimScale-f1} and~\ref{fig:carti-dimScale-e4sc}. The quality of the clusters found by \casclud is not affected by the number of dimensions. This is expected, because more dimensions only add more views for our method without corrupting the existing views. Therefore, our transformation truly preserves the neighborhood information and its cluster detection capability does not degrade with an increase in the dimensionality.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_f1}
	\end{center}
	\caption{F1 Scores for datasets with increasing number of dimensions. In all of the datasets dimensionality of clusters are proportional to the total number of dimensions.}
	\label{fig:cart-dimScale-f1}
\end{figure}
\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_dimScale_e4sc}
	\end{center}
	\caption{E4SC Scores for datasets with increasing number of dimensions.}
	\label{fig:cart-dimScale-e4sc}
\end{figure}

\subsection{Noise Awareness}
Evaluation of noise awareness is done using a set of datasets that contain a fixed number of clusters and dimensions but include different ratios of noise. Datasets N30, N50 and N70 have 30\%, 50\% and 70\% of noise objects, respectively.%~\cite{muller:09:sscl}.

Results in Figures~\ref{fig:cart-noiseScale-f1} and~\ref{fig:cart-noiseScale-e4sc} show that \casclud effectively detects high quality clusters, even if they exist in only 30\% of the data.
Since random objects are not nearest neighbors of most clustered objects in many views of the data, they are not expected to be in many carts, therefore, the support of object sets that they are part of are low.
While \casclud detects structure it ignores objects with low supports, i.e., noise. In contrast to this robust behavior, we observe the quality of the clusters found by K-Means, Proclus, and CSPA degenerate with increasing noise.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_noiseScale_f1}
	\end{center}
	\caption{F1 Scores for datasets with increasing amounts of noise. 30\%, 50\%, and 70\% of the objects in these datasets do not form cluster structures.}
	\label{fig:cart-noiseScale-f1}
\end{figure}

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_noiseScale_e4sc}
	\end{center}
	\caption{E4SC Scores for datasets with increasing amounts of noise.}
	\label{fig:cart-noiseScale-e4sc}
\end{figure}


\subsection{Subspace Detection}
To evaluate the correct detection of subspaces, we generate datasets with 10 hidden clusters having 2 to 7 relevant dimensions and add additional random dimensions. The datasets ND10, ND50, and ND200 have respectively 10, 50 and 200 irrelevant dimensions. All of these datasets include additional 5\% random objects as noise.

Results in Figures~\ref{fig:carti-nd-f1} and~\ref{fig:carti-nd-e4sc} clearly show the benefits of aggregating information from multiple views of the data.
\casclud and CSPA can extract and efficiently use the information from the subspaces that contain structures to overcome the negative effects of irrelevant dimensions.
Thanks to the stricter cluster definition of \casclud, i.e., \emph{co-occurrence} instead of pair similarities, and noise detection capabilities, it can detect better clusters than CSPA.
Proclus can ignore the the irrelevant dimensions up to some level. However, its greedy approach combined with random initialization cannot compensate the degrading effects of the increasing number of irrelevant dimensions.
Since feature selection techniques treat all dimensions equally, increasing the number of irrelevant dimensions renders them useless.
Note that, since \emph{CSPA} cannot report subspaces for clusters, its \emph{E4SC} score decrease with the number of dimensions.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_noiseDim_f1}
	\end{center}
	\caption{F1 scores of the methods on datasets with an increasing number of irrelevant dimensions. 10 clusters of 3 to 6 dimensions are hidden 10, 50, and 200 additional uniformly distributed dimensions.}
	\label{fig:carti-nd-f1}
\end{figure}
\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.95\textwidth,keepaspectratio=true,draft=false]{figs/chart_noiseDim_e4sc}
	\end{center}
	\caption{E4SC scores of the methods on datasets with an increasing number of irrelevant dimensions.}
	\label{fig:carti-nd-e4sc}
\end{figure}
%\begin{figure*}[t]
%	\begin{center}
%		\subfloat[10 Noise Dimensions (10c20d)]{\includegraphics[width=0.48\textwidth,keepaspectratio=true,draft=false]{figs/chart_10c20d}}
%		\subfloat[50 Noise Dimensions (10c60d)]{\includegraphics[width=0.48\textwidth,keepaspectratio=true,draft=false]{figs/chart_10c60d}}
%		\\
%		\subfloat[200 Noise Dimensions (10c210d)]{\includegraphics[width=0.48\textwidth,keepaspectratio=true,draft=false]{figs/chart_10c210d}}
%	\end{center}
%%\vspace{-1.5em}
%	\caption{Increasing the number of noise (irrelevant) dimensions of datasets that have clusters in 10 dimensions.}
%	\label{fig:noise-dims}
%\end{figure*}




\subsection{Parameter Sensitivity}\label{section:experiments-param}
The results of the experiments evaluating the sensitivity to the values of \minsupratio and \kratio with regard to the different ratios of noise objects and noise dimensions are shown as Figure~\ref{fig:param}.
We see that overall \kratio is robust, yet, as explained in Section~\ref{section:application}, the optimal value depends on the amount of noise in the data.
While decreasing the \kratio decrease the size of the detected clusters, and hence, increase purity,
higher values for \kratio extracts larger clusters.
We further see that \minsupratio is very robust in terms of cluster purity, while increasing its value decreases the size of clusters, which, in turn, results in lower \emph{F1} scores.

\begin{figure*}[t!]
	\begin{center}
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_1_1}}
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_1_2}}
		\\
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_1_3}}
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_1_4}}
		\\
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_2_1}}
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_2_2}}
		\\
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_2_3}}
		\subfloat{\includegraphics[width=0.40\textwidth,keepaspectratio=true,draft=false]{figs/chart_param_2_4}}
	\end{center}
	\caption{Effects of parameters over the quality of detected clusters.}
	\label{fig:param}
\end{figure*}

\subsection{Real World Data}
Next we evaluate the performance of \casclud on real world data. As real data, we here consider three datasets. First, the \emph{Vowel} recognition dataset consists of 11 attributes and 990 objects, spread over 10 classes of 99 instances for each. Second, the \emph{Shape} recognition dataset has 160 instances over 17 attributes grouped under 9 known clusters. These two datasets are publicly available as benchmarks for subspace clustering~\cite{mueller2009eval}.
Third, we use the publically available \emph{Alon} Colon Cancer dataset~\cite{alon} dataset. This is a gene expression dataset of 2000 attributes and one binary class for 62 tissue samples.

As for none of these datasets there exists a known ground truth for the subspaces, we here only consider F1 scores. Figure~\ref{fig:real-data} gives the performance of the six methods over these datasets. We see that for the small \emph{Shape} dataset \casclud performs on par with its competitors. For \emph{Vowel} and \emph{Alon} it shows better performance. For the latter, the most high-dimensional dataset we consider, \casclud obtains F1 scores that are 25\% higher than \emph{Proclus} and 10\% higher than \emph{CSPA}, respectively.

Note that in real data, cluster structures do not necessarily have to correlate with the class labels, for all methods the F1 scores in this experiment are not as high as for the synthetic data.

\begin{figure}[tb]
	\begin{center}
		\includegraphics[width=0.80\textwidth,keepaspectratio=true,draft=false]{figs/chart_real}
	\end{center}
%\vspace{-1.5em}
\caption{Real World Datasets}
	\label{fig:real-data}
\end{figure}
