\section{Ranked Cartification}
In the approach studied in this chapter, we will exploit the relative similarity of objects
while keeping the advantages of neighborhood approach.
We achieve this by defining neighborhoods as ranked vectors instead of sets
and hence keep the orders in the neighborhoods.
We claim that clusters can be identified more robustly in these ranked neighborhoods than the binary ones.

We will first formalize the problem of finding clusters in ranked data, followed by a new type of \findclusters algorithm that finds this type of cluster.

\subsection{Problem Definition}
\label{sec:ranked-matrix}
Following the transformation idea of cartification, we transform a relational database into a ranked neighborhood matrix,
and then we use this matrix to detect subspace clusters.


First we extend the definition of neighborhood and take the order in the neighborhood into account.

\begin{figure*}[bt]
	\begin{center}
		\includegraphics[width=.8\textwidth,keepaspectratio=true,draft=false]{figs/example}
	\end{center}
	\caption{Example Neighborhoods}
	\label{fig:example}
\end{figure*}

\begin{definition}[Ordered Neighborhood]
	\emph{Ordered neighborhood} of an object $\voi \in \DB$, $\neigh{\voi}$, is a vector of objects that are ordered by their similarity to \voi. 
	For $\voi, \voj, \vok \in \DB$ and a dissimilarity \dist,
	\[
	\neigh{\voi} = (\cdots,\voj,\cdots,\vok,\cdots) \iff \dist(\voi,\voj) < \dist(\voi,\vok)
	\]
\end{definition}

Ordered neighborhoods represent the relative similarities of pairs of objects in the dataset;
while they are still unaffected by the scale.
Figure~\ref{fig:example}a shows the \emph{ordered neighborhoods} of $\vo_{1}$, $\vo_{2}$, and $\vo_{6}$ in Table~\ref{tab:example-db}.
The closest data object to $\vo_{1}$ is the $\vo_{1}$ itself, therefore it is the first object in $\neigh{\vo_{1}}$,
followed by $\vo_{4}$ which is the second closest one.
Note that the $k$-nearest neighborhood of \voi is the first $k$ elements of $\neigh{\voi}$.

\begin{definition}[Ranked Neighborhood]
	\label{def:ranked-neighborhood}
	The \emph{Ranked Neighborhood} of an object $\vo_i$ is an $n$-dimensional vector, where $n=|\DB|$.
	The $j^{th}$ value of the vector is defined as
\[
	\mathbf{N}(\vo_{i})_{j} = |\{\vo_{k} \in \DB | \dist(\vo_{i}, \vo_{k}) < \dist(\vo_{i}, \vo_{j})  \}|,
	\]
	i.e. 
		each dimension $j$ represents the number of objects between the object $\vo_i$ and the object $\vo_j$ in the ordered neighborhood.
	%the number of objects between the respective object and the reference object.
\end{definition}

Figure~\ref{fig:example}b shows the \emph{ranked neighborhood} of $\vo_{6}$.
The columns of the vector represent the objects 1 through 12.
For example, since $\vo_{1}$ is in the 10th position in $\neigh{\vo_{6}}$, the first value in the vector is 10.

Based on the ranked neighborhoods we define a ranked neighborhood matrix.


\begin{definition}[Ranked Neighborhood Matrix]
	\label{def:ranked-matrix}
	The \emph{Ranked Neighborhood Matrix} is the matrix of ranked neighborhoods, i.e., it is the matrix
	\[\mM=
	\left( \begin{array}{ccc}
	\rule[.5ex]{3.5em}{0.4pt} & \mathbf{N}(\vo_{1}) & \rule[.5ex]{3.5em}{0.4pt}\\
	\rule[.5ex]{3.5em}{0.4pt} & \mathbf{N}(\vo_{2}) & \rule[.5ex]{3.5em}{0.4pt}\\
	&\vdots&\\
	\rule[.5ex]{3.5em}{0.4pt} & \mathbf{N}(\vo_{n}) & \rule[.5ex]{3.5em}{0.4pt}\\
		\end{array} \right)
	\]
\end{definition}

Table~\ref{tab:toy-rank} shows the \emph{ranked neighborhood matrix} for the dataset in Table~\ref{tab:example-db}.
It has 12 rows and 12 columns representing the objects in the dataset.
Each row is the ranked vector of the respective object:
compare the row of $\vo_{6}$ to Figure~\ref{fig:example}b.


\begin{table}[tb]
\setlength\tabcolsep{5pt}
	\scriptsize
	\centering
	\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
			& $\vo_{1}$ & $\vo_{2}$ & $\vo_{3}$ & $\vo_{4}$ & $\vo_{5}$ & $\vo_{6}$ & $\vo_{7}$ & $\vo_{8}$ & $\vo_{9}$ & $\vo_{10}$ & $\vo_{11}$ & $\vo_{12}$ \\ \hline
		$\vo_{1}$ & 0&5&3&1&8&7&4&2&9&6&10&11 \\ \hline
		$\vo_{2}$ & 4&0&2&3&8&7&1&5&9&6&10&11 \\ \hline
		$\vo_{3}$ & 4&2&0&3&8&7&1&5&9&6&10&11 \\ \hline
		$\vo_{4}$ & 1&5&3&0&8&7&4&2&9&6&10&11 \\ \hline
		$\vo_{5}$ & 10&6&8&9&0&2&7&11&1&3&4&5 \\ \hline
		$\vo_{6}$ & 10&6&8&9&1&0&7&11&2&3&4&5 \\ \hline
		$\vo_{7}$ & 4&2&1&3&8&7&0&5&9&6&10&11 \\ \hline
		$\vo_{8}$ & 1&5&3&2&8&7&4&0&9&6&10&11 \\ \hline
		$\vo_{9}$ & 10&6&8&9&1&2&7&11&0&3&4&5 \\ \hline
		$\vo_{10}$ &10&6&8&9&2&1&7&11&3&0&4&5 \\ \hline
		$\vo_{11}$ &10&6&8&9&3&4&7&11&2&5&0&1 \\ \hline
		$\vo_{12}$ &10&6&8&9&3&4&7&11&2&5&1&0 \\ \hline
	\end{tabular}
	\caption{Ranked Neighborhood Matrix for Table~\ref{tab:example-db}}
	\label{tab:toy-rank}
\end{table}

The ranked matrix preserves the neighborhood relations in the original dataset.
If a set of objects are close to each other, their respective \emph{rank}s will be under a certain threshold.
In Table~\ref{tab:toy-rank}, the mutual ranks of the objects 1, 2, 3, 4, 7, 8 are all below 6.
The same observation holds for the objects 4, 6, 9--12.
Considering the original dataset, we can see that both of these object sets form clusters.
Therefore, we can detect cluster structures by finding the minimum values in a ranked matrix.

Note the similarity to \emph{cartification}: the objects that have a ranking score below a certain threshold are the \emph{neighbors} of the respective object.
For example, the objects in $\knn{6}{\vo_1}$, cf.\ Table~\ref{tab:neighborhood-db}, have values below 6 at the first row of Table~\ref{tab:toy-rank}.
Nevertheless, in contrast to the set-based approach of cartification's neighborhoods,
ranked neighborhoods preserve the similarity information \emph{in} the neighborhoods.
Using the complete neighborhood information, we can make the cluster detection more robust, without fixing a threshold $k$.

The idea is to formalize the discovery of clusters as the discovery of {\em tiles} of low rank in the ranked neighborhood matrix.

\begin{definition}[Tile]
Given a set of object identifiers $O$,
a {\em tile} in a ranked neighborhood matrix $\mM$ is a square submatrix of the ranked neighborhood matrix. I.e., for a given set of object identifiers $O$, it consists of all cells $\mM_{ij}$ of $\mM$ with $i, j\in O$.

We define the quality of a tile identified by a set of objects $O$ as
\[
f(O)=\sum_{i \in O, j \in O} (\mM_{ij} - \theta),
\]
where $\theta$ is a scalar value for thresholding.

A {\em minimal tile} is a tile that has minimal score $f(O)$.
\end{definition}
The idea behind the scoring function is that cells in the ranked neighborhood matrix with a value higher than the threshold will contribute a positive term to the summation, while cells below the threshold contribute a negative term. Clusters are preferred that have as many cells below the threshold $\theta$ in the corresponding tile.

Note that in cartification, we need to solve this minimization problem multiple times, as we need to solve it for the different attributes independently. The \findclusters implementation for ranked cartification hence solves the problem of finding the minimal tile for each attribute independently, while restricting the search to only those objects that it is allowed to put in a cluster, as identified by the $O$ parameter of the \findclusters function.

Indirectly, the parameter $\theta$ influences the size of the clusters that can be found. In our experimental evaluation, we will compare the effect of this parameter to that of the $k$ parameter in the original cartification method.

\subsection{Properties}
The problem of ranked tiling was studied in earlier work by Le Van et al.~\cite{levan}. However, given the origin of the ranked matrix studied in ranked cartification, we can use a more efficient algorithm in this particular setting. This algorithm relies on the particular properties of the sorted ranked neighborhood matrix.



\begin{definition}[Sorted Ranked Neighborhood Matrix]
	\label{def:sorted-matrix}
	Given an attribute $A_d$,
	the \emph{sorted ranked neighborhood matrix} for this attribute is the ranked neighborhood matrix obtained by sorting the objects in the rows and columns according to their value on attribute $A_d$.
\end{definition}

For example, if we sort the rows and columns of the ranked neighborhood matrix in Table~\ref{tab:toy-rank} according to the object values in $A_1$, then we will have the matrix in Table~\ref{tab:toy-transform-ordered}.

\setlength\tabcolsep{5pt}
\begin{table}[tb]
	\scriptsize
	\centering
		\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
			&$\vo_{12}$& $\vo_{11}$& $\vo_{9}$& $\vo_{5}$& $\vo_{6}$& $\vo_{10}$& $\vo_{2}$& $\vo_{7}$& $\vo_{3}$& $\vo_{4}$& $\vo_{1}$& $\vo_{8}$ \\ \hline
			$\vo_{12}$& 0&1&2&3&4&5&6&7&8&9&10&11 \\ \hline
			$\vo_{11}$& 1&0&2&3&4&5&6&7&8&9&10&11 \\ \hline
			$\vo_{9}$&  5&4&0&1&2&3&6&7&8&9&10&11 \\ \hline
			$\vo_{5}$&  5&4&1&0&2&3&6&7&8&9&10&11 \\ \hline
			$\vo_{6}$&  5&4&2&1&0&3&6&7&8&9&10&11 \\ \hline
			$\vo_{10}$& 5&4&3&2&1&0&6&7&8&9&10&11 \\ \hline
			$\vo_{2}$&  11&10&9&8&7&6&0&1&2&3&4&5 \\ \hline
			$\vo_{7}$&  11&10&9&8&7&6&2&0&1&3&4&5 \\ \hline
			$\vo_{3}$&  11&10&9&8&7&6&2&1&0&3&4&5 \\ \hline
			$\vo_{4}$&  11&10&9&8&7&6&5&4&3&0&1&2 \\ \hline
			$\vo_{1}$&  11&10&9&8&7&6&5&4&3&1&0&2 \\ \hline
			$\vo_{8}$&  11&10&9&8&7&6&5&4&3&2&1&0 \\ \hline
		\end{tabular}
		\caption{Ordered ranked neighborhood matrix for $a_{1}$}
		\label{tab:toy-transform-ordered}
\end{table}


The following properties hold for a \emph{sorted ranked neighborhood matrix} $\mM$:

\begin{property}
	\label{pro:diagonal}
	For all $i$: $\mM_{ii}=0.$
\end{property}
\begin{proof}
	The diagonals are always zero since the order of rows and columns is the same and each point is most similar to itself.
\end{proof}
\begin{property}
	\label{pro:row}
	In a row, minimum values are next to each other and this neighborhood involves the diagonal.
	Formally,
	for the values in the $i^{th}$ row, the following statements hold:
	\begin{enumerate}
	\item for $j$ and $k$ with $i<j<k$: $\mM_{ij} < \mM_{ik}$;
	\item for $j$ and $k$ with $j<k<i$: $\mM_{ij} > \mM_{ik}$.
	\end{enumerate}
\end{property}
\begin{proof}
	By definitions~\ref{def:ranked-neighborhood} and~\ref{def:ranked-matrix}, each row of \mM is a ranked neighborhood, i.e., $\mM_{ij} = \mathbf{N}(\vo_{i})_{j}$.
	Since $\mM$ is a \emph{sorted} ranked neighborhood matrix, if the columns stand for the objects
	$\vo_{a_0}, \vo_{a_1}, \cdots, \vo_{a_n}$, then $i < j \implies \vo_{a_i} <= \vo_{a_j}$.
	Consequently, for $ \vo_{a_i} < \vo_{a_j} < \vo_{a_k}$ and $\vo_{a_x} \in \DB$,
	\[
	|\{\vo_{a_x}| \dist(\vo_{a_i}, \vo_{a_x}) < \dist(\vo_{a_i}, \vo_{a_j})  \}|  <
	|\{\vo_{a_x}| \dist(\vo_{a_i}, \vo_{a_x}) < \dist(\vo_{a_i}, \vo_{a_k})  \}|
	\]
	and therefore $i < j < k \implies \mM_{ij} < \mM_{ik}$.
	Second statement of the property can be proven in a similar way.

\end{proof}
We prove a similar property for the columns.
\begin{property}
	\label{pro:col}
For the values in the $i^{th}$ column, it holds that:
	\begin{itemize}
	\item for $j$ and $k$ with $i<j<k$: $\mM_{ji} \leq \mM_{ki}$;
	\item for $j$ and $k$ with $j<k<i$: $\mM_{ji} \geq \mM_{ki}$.
	\end{itemize}
\end{property}
\begin{proof}
	Similar to the proof of Property~\ref{pro:row}, $i<j<k \implies \vo_{a_i} < \vo_{a_j} < \vo_{a_k} \implies \mM_{ji} \le M_{ki}$.
	Here, the additional argument is that by moving lower down or higher up from a particular $\mM_{ii}$,
	the number of objects ranked lower than the object in column $i$ can only increase,
	as the objects lower down and higher up the matrix for this column are farther away from the object $\voi$.
\end{proof}
From these properties follows an important new property that allows us to improve the search significantly:
\begin{property}
	\label{pro:cont}
	Minimal tiles are contiguous, i.e., the set of objects chosen in a minimal tile $O$ always consists of a range $O=\{i,i+1,\ldots,j-1,j\}$ for some $1\leq i\leq j\leq n$.
\end{property}
\begin{proof}
	This follows from the Properties~\ref{pro:row} and~\ref{pro:col}.
\end{proof}


All of these properties can be observed in Table~\ref{tab:toy-transform-ordered}:
(1) diagonals are all zero,
(2) the value for $\vo_{5}$ at the row of $\vo_{6}$ is smaller than the value for $\vo_{9}$, because $\vo_{5}$ is closer to the diagonal,
(3) similarly, the value for $\vo_{5}$ is smaller at the row of $\vo_{2}$ than the value for $\vo_{5}$ at the row of $\vo_{6}$.

\subsection{Algorithm}
Property~\ref{pro:cont} allows us to develop an efficient search algorithm: instead of searching over all possible subsets of objects, we search over all possible combinations of $i$ and $j$ with $1\leq i\leq j\leq n$. This yields the algorithm in 
Algorithm~\ref{algo:square} for finding a minimal tile. 
\tiler takes the sorted ranked neighborhood matrix and the threshold $\theta$ as parameter.
Exploiting the fact that ranks will only increase when increasing $j$, it stops growing a tile if the new addition of values does not decrease the sum (lines 8 and 9).
This optimization exploits Properties~\ref{pro:row} and~\ref{pro:col}.

\begin{algorithm}[tb]
	\begin{algorithmic}[1]
		\REQUIRE \mM: Rank Matrix, $\theta$
		\ENSURE $min\_t$: Minimum tile

		\STATE $\textit{min\_t} := (0,0)$ \COMMENT{Minimum tile}
		\STATE $\textit{min\_ts} := -\theta$ \COMMENT{Minimum tile sum}
		\FOR{$i$ := $1$ \textbf{to} $n$}
			\STATE $ts := \mM_{ii}-\theta$ \COMMENT{Tile sum}
			\FOR{$j := i+1$ \textbf{to} $n$}
				\STATE $ns := (\mM_{ij}-\theta) + (\mM_{(i+1)j}-\theta) + \cdots + (\mM_{jj}-\theta)$
				\STATE $~~~+ (\mM_{ji}-\theta) + (\mM_{j(i+1)}-\theta) + \cdots + (\mM_{j(j-1)}-\theta)$

				\IF{$ns > 0$}
					\STATE \textit{Continue with the next $i$}
				\ENDIF
				\STATE $ts := ts + ns$

				\IF{$ts < min\_ts$}
					\STATE $min\_ts := ts$
					\STATE $min\_t := (i,j)$
				\ENDIF
			\ENDFOR
		\ENDFOR
		\RETURN $min\_t$

	\end{algorithmic}
	\caption{\tiler: Tiling on Ordered Ranked Matrix}
	\label{algo:square}
\end{algorithm}

