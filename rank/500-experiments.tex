\section{Experiments}
\label{sec:experiments}
In this section, we empirically evaluate the cluster finding capabilities of \cartirank.
We compare the robustness of \cartirank and Cartification with respect to parameter settings and data distributions. The overall cluster quality is evaluated in comparison with other state-of-the-art subspace clustering methods.
For the reproducibility of the results, we provide the implementation and the datasets on our web site.\!\footnote{\url{http://adrem.uantwerpen.be/cartirank}}

\subsection{Robust Cluster Detection}
We compare the cluster detection capabilities of Cartification and \cartirank.
Both of the algorithms exploit one-dimensional projections to detect clusters.
Therefore, they should be able to find clusters with various sizes and properties in projections without requiring strict parameter settings.
%requiring non-strict parameters.
In other words, the quality of the clusters should be robust to both parameters and the characteristics of the data.
To evaluate the robustness of one-dimensional cluster finding capabilities, we generate 6 synthetic datasets.
Properties of these dataset are shown in Table~\ref{tab:robustness-data}.
\textbf{ns25} and \textbf{ns50} have three clusters that are not separable, i.e., the diameters of the clusters are larger than the distances between clusters.
\textbf{s3} has 3 clusters with sizes 25, 50, and 25.
\textbf{vs4}, \textbf{vs5}, and \textbf{vs6} have clearly separated clusters of different sizes.


\begin{table}
	\centering
	\begin{tabular}{|c|>{\centering\arraybackslash}p{2cm}|c|>{\centering\arraybackslash}p{2cm}|>{\centering\arraybackslash}p{2cm}|}
		\hline
		Name & \# of clusters & Cluster Sizes & Cluster Radius & Distance Between Clusters \\
		\hline\hline
		\textbf{ns25} & 3 & 25-25-25 & 4 & 3 \\  \hline
		\textbf{ns50} & 3 & 50-50-50 & 4 & 3 \\  \hline
		\textbf{s3} & 3 & 25-50-25 & 4 & 4 \\  \hline
		\textbf{vs4} & 4 & 25-40-55-70 & 4 & 8 \\  \hline
		\textbf{vs5} & 5 & 50-25-50-25-50 & 4 & 8 \\  \hline
		\textbf{vs6} & 6 & 15-30-45-45-30-15 & 4 & 8 \\  \hline
	\end{tabular}
	\caption{Datasets for robustness tests}
	\label{tab:robustness-data}
\end{table}


On these 6 datasets, we run Cartification~\cite{clon} and \cartirank
with different neighborhood sizes, namely $\theta$ and $k$, and report the quality of the found clusters.
Since we know the true clusters beforehand, we use the supervised F1 score to assess the quality of the found clusters.
The F1 score is the harmonic mean of the  \textit{precision} and \textit{recall} between two cluster sets.
\textit{Precision} between a known cluster $C_{1}$ and a detected cluster $C_{2}$ is the size of the intersection of $C_{1}$ and $C_{2}$ divided by the size of $C_{2}$.
Likewise, \textit{recall} is the number of common objects between $C_{1}$ and $C_{2}$ divided by the size of $C_{1}$.
We map each known cluster to the detected cluster which produces the maximum F1 score and take the average of the scores.
In other words, we measure whether the known clusters are among the clusters found using \cartirank and Cartification.
What we call F1 in this chapter is called \emph{F1-Recall} in Section~\ref{section:experiments}.

Figure~\ref{fig:quality} shows the F1 scores of the algorithms.
We see the benefits of using the similarity information in the neighborhoods, i.e. using ranks, on all of the datasets:
\cartirank produces better quality clusters for a wider range of $\theta$ values.
When the clusters are the same size but not separated, Cartification can detect the perfect clusters only for one parameter value,
while \cartirank can produce the perfect clustering for a wide range of values.

The benefits are more visible on datasets that have clusters of various sizes, namely \textbf{vs4} and \textbf{vs6}.
%The effect of robustness can be seen on the datasets that have clusters of various sizes.
Given that $k$ defines a limit on the size of clusters that can be found,
cartification can find only one cluster size at a time, and thus, can never find all of the clusters at once.
\cartirank can produce a perfect clustering for all of the datasets.
\begin{figure*}[tb]
	\begin{center}
		\subfloat[\textbf{ns25}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_ns25}
		}
		\subfloat[\textbf{ns50}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_ns50}
		}
		\\
		\subfloat[\textbf{s3}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_s3}
		}
		\subfloat[\textbf{vs4}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_vs4}
		}
		\\
		\subfloat[\textbf{vs5}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_vs5}
		}
		\subfloat[\textbf{vs6}]{
			\includegraphics[width=0.5\textwidth,keepaspectratio=true,draft=false]{figs/chart_F1R_vs6}
		}
	\end{center}
	\caption{F1 Scores of the methods for a range of parameters}
	\label{fig:quality}
\end{figure*}

Our experiments with a wide range of values for the minimum length parameter $\mu$ show that the effect of this parameter is negligible.
For the same $\theta$, in these experiments, the $\mu$ parameter does not change the outcome at all,
or changes the F1 score of the found clusters not more than 0.05.
%Of course, as long as it is below the expected cluster size.

\subsection{Subspace Clustering}

To evaluate subspace cluster finding capabilities of the methods, we generated a set of datasets.
Each dataset has 5 sets of overlapping clusters which are hidden in 5 sets of attributes.
An object can be a member of one cluster according to one attribute and a different cluster in another attribute.

An example cluster formation is visualized in Figure~\ref{fig:subspace},
in which the columns represent attributes and the rows represent objects.
Cluster assignments in different attributes are shown as $C_i$.
For example $\vo_{1}$ and $\voi$ are in cluster $C_1$ according to attributes $A_{1}$ and $A_{2}$, but they are in different clusters in $A_{k}$.

We generate datasets with similar characteristics multiple times, run the algorithms on them and report the average results.
Generation parameters for the datasets are shown in Table~\ref{tab:subspace-datasets}.
After generating datasets using these parameters, we add 2 redundant dimensions with random values and add 5\% random noise to them.


\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Name & \# of clusters & Attributes/cluster & Cluster Sizes & \# of Attributes \\ \hline
		\textbf{SVS-1} & 15-25 & 2-6 & 50-100 & 10-30\\ \hline
		\textbf{SVS-2} & 15-25 & 2-6 & 50-150 & 10-30 \\ \hline
		\textbf{SVS-3} & 15-25 & 2-5 & 50-200 & 10-30 \\ \hline
	\end{tabular}
	\caption{Generation parameters for subspace cluster datasets}
	\label{tab:subspace-datasets}
\end{table}

The F1 scores of the found clusters for a range of parameters are shown in Figure~\ref{fig:subspace-clusters-f1}.
As in one-dimensional clustering, the qualities of the clusters produced by \cartirank are always in a certain interval.
For the optimal parameters settings, the quality of the found clusters are comparable.
Here is the catch:
Figure~\ref{fig:subspace-clusters-sizes} shows the number of clusters found by each of the methods.
We can see that cartification outputs so many clusters that they can not be put into a good use in practical scenarios.
Clusters found by \cartirank are orders of magnitude less redundant.


We compare the quality of \cartirank with two of the state-of-the-art subspace clustering algorithms.
Figure~\ref{fig:vs-other} shows the F1 scores of \cartirank, Cartification, PROCLUS~\cite{PROCLUS}, and STATPC~\cite{STATPC} for subspace clustering datasets.
We optimized the parameters of the algorithms for the best results.
As expected, PROCLUS can cope neither with overlapping clusters nor with noise.
STATPC uses an approximation and for our datasets it does not work well.
Moreover, optimizing the complex parameters of STATPC is not trivial.


\begin{figure}[tb]
	\begin{center}
			\includegraphics[width=.9\textwidth,keepaspectratio=true,draft=false]{figs/chart_vs_other}
	\end{center}
	\caption{The best clusters found by the different algorithms}
	\label{fig:vs-other}
\end{figure}


%Since we do not have class labels, found clusters and known classes should be matched.
%We do this in two ways: (1) Match each found cluster to the known class that gives the maximum F1 value,
%(2) Match each known class to the found cluster that gives the maximum F1 value.
%We call the first one \textit{F1 Precision} and the second one \textit{F1 Recall}.
%The difference between matching is shown in Figure~\ref{fig:f1-dif}.

\begin{figure}[tb]
	\begin{center}
			\includegraphics[width=0.60\textwidth,keepaspectratio=true,draft=false]{figs/subspace}
	\end{center}
	\caption{Subspace clusters in synthetic datasets}
	\label{fig:subspace}
\end{figure}


\begin{figure}[tb]
	\begin{center}
		\subfloat[SVS-1]{
			\includegraphics[width=0.70\textwidth,keepaspectratio=true,draft=false]{figs/chart_svs1_f1}
		}
		\\
		\subfloat[SVS-2]{
			\includegraphics[width=0.70\textwidth,keepaspectratio=true,draft=false]{figs/chart_svs2_f1}
		}
		\\
		\subfloat[SVS-3]{
			\includegraphics[width=0.70\textwidth,keepaspectratio=true,draft=false]{figs/chart_svs3_f1}
		}
	\end{center}
	\caption{Quality of the subspace clusters}
	\label{fig:subspace-clusters-f1}
\end{figure}

\begin{figure}[tb]
	\begin{center}
		\subfloat[SVS-2]{
			\includegraphics[width=0.70\textwidth,keepaspectratio=true,draft=false]{figs/chart_svs2_sizes}
		}
		\\
		\subfloat[SVS-3]{
			\includegraphics[width=0.70\textwidth,keepaspectratio=true,draft=false]{figs/chart_svs3_sizes}
		}
	\end{center}
	\caption{The number of clusters found by the methods}
	\label{fig:subspace-clusters-sizes}
\end{figure}
