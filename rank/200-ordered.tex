\section{Clustering in Ranked Neighborhoods}
\label{sec:rank-ordered}
\subsection{Ordered Neighborhoods}
For a \textit{relational database} \DB we represent each data object \vo in \DB as a vector defined over attributes $\Ats = \{A_1, A_2, \cdots, A_m\}$, with $\vo_{i}$ the value of the object for the attribute $A_{i}$.
%For each attribute $A_{i}$, the projected database is denoted as $\DB^{A_i}$. 
Further, we use $\dist(\vo,\vp)$ as a dissimilarity between the objects \vo and \vp.
$|S|$ denotes the cardinality of set $S$.


\begin{definition}[Ordered Neighborhood]
	\emph{Ordered neighborhood} of an object $\vo \in \DB$, $\neigh{\vo}$, is a list of items that are ordered by their similarity to \vo. 
	For $\vo, \vp, \vq \in \DB$ and a dissimilarity \dist,
	\[
	\neigh{\vo} = (\cdots,\vp,\cdots,\vq,\cdots) \iff \dist(\vo,\vp) < \dist(\vo,\vq)
	\]
\end{definition}

Figure~\ref{fig:example}a shows a simple database which consists of 12 objects and one attribute.
\emph{ordered neighborhoods} of $\vo_{1}$, $\vo_{2}$, and $\vo_{6}$ are shown in Figure~\ref{fig:example}b.
The closest data object to $\vo_{1}$ is the $\vo_{1}$ itself, therefore it is the first object in $\neigh{\vo_{1}}$,
followed by $\vo_{4}$ which is the second closest one.

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=1\columnwidth,keepaspectratio=true,draft=false]{figs/example}
	\end{center}
	\caption{Neighborhood example}
	\label{fig:example}
\end{figure}

\begin{definition}[Ranked Neighborhood]
	\emph{Ranked Neighborhood} of an object is an $n$ dimensional vector, where $n=|\DB|$.
	Each dimension represents an object and the cell value is its proximity order from the reference object.
	Formally,
	\[
	\mathbf{N}(\vo_{i})_{j} = |\{\vo_{k} \in \DB | \dist(\vo_{i}, \vo_{k}) < \dist(\vo_{i}, \vo_{j})  \}| 
	\]

	%the number of objects between the respective object and the reference object.
\end{definition}

Figure~\ref{fig:example}c shows the \emph{ranked neighborhood} of $\vo_{6}$.
Columns of the vector represent the objects 1 through 12.
For example, since $\vo_{1}$ is in the 10th position in $\neigh{\vo_{1}}$, first value in the vector is 10.

\begin{definition}[Ranked Neighborhood Matrix]
	\emph{Ranked Neighborhood Matrix} represents the ranked neighborhoods of all object for a dissimilarity measure \dist.
	It is a square matrix where rows and columns represent objects.
	Cell values are the respective neighborhood orders of the objects in columns according to objects in rows.
\end{definition}

Table~\ref{tab:toy-rank} shows the \emph{ranked neighborhood matrix} for the dataset in Figure~\ref{fig:example}a.
It has 12 rows and 12 columns representing the objects in the dataset.
Each row is the ranked vector of the respective object.


\begin{table}[ht]
\setlength\tabcolsep{5pt}
	\scriptsize
	\centering
	%\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
	\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
			& $\vo_{1}$ & $\vo_{2}$ & $\vo_{3}$ & $\vo_{4}$ & $\vo_{5}$ & $\vo_{6}$ & $\vo_{7}$ & $\vo_{8}$ & $\vo_{9}$ & $\vo_{10}$ & $\vo_{11}$ & $\vo_{12}$ \\ \hline
		$\vo_{1}$ & 0&5&3&1&8&7&4&2&9&6&10&11 \\ \hline
		$\vo_{2}$ & 4&0&2&3&8&7&1&5&9&6&10&11 \\ \hline
		$\vo_{3}$ & 4&2&0&3&8&7&1&5&9&6&10&11 \\ \hline
		$\vo_{4}$ & 1&5&3&0&8&7&4&2&9&6&10&11 \\ \hline
		$\vo_{5}$ & 10&6&8&9&0&2&7&11&1&3&4&5 \\ \hline
		$\vo_{6}$ & 10&6&8&9&1&0&7&11&2&3&4&5 \\ \hline
		$\vo_{7}$ & 4&2&1&3&8&7&0&5&9&6&10&11 \\ \hline
		$\vo_{8}$ & 1&5&3&2&8&7&4&0&9&6&10&11 \\ \hline
		$\vo_{9}$ & 10&6&8&9&1&2&7&11&0&3&4&5 \\ \hline
		$\vo_{10}$ &10&6&8&9&2&1&7&11&3&0&4&5 \\ \hline
		$\vo_{11}$ &10&6&8&9&3&4&7&11&2&5&0&1 \\ \hline
		$\vo_{12}$ &10&6&8&9&3&4&7&11&2&5&1&0 \\ \hline
	\end{tabular}
	\caption{Ranked Neighborhood Matrix for Figure~\ref{fig:example}a}
	\label{tab:toy-rank}
\end{table}

Ranked matrix preserves the neighborhood relations in the original dataset.
If a set of objects are close to each other, their respective \emph{rank}s will be under a certain threshold.
In Table~\ref{tab:toy-rank}, mutual ranks of objects 1, 2, 3, 4, 7, 8 are all below 6.
The same observation holds for the objects 4, 6, 9--12.
Considering the original dataset, we can see that both sets of these objects form clusters.
Therefore, we can detect cluster structures by finding the minimum values in a ranked matrix.

\emph{Cartification}~\cite{cartification} exploits this property of ranked matrices to detect clusters.
It applies a hard thresholding on the ranked matrix and create a binary neighborhood dataset.
Although this technique has advantages, it cannot cope with clusters of different sizes and characteristics as we study in Section~\ref{sec:experiments}.

Using the complete neighborhood information, we can make the cluster detection more robust.
Generic ranked matrices are studied in the literature.
In a recent study, Le Van et al.~\cite{levan} propose an algorithm to find interesting \emph{tile}s, i.e. areas, in a ranked matrix.
Proposed algorithm transforms the ranked matrix by subtracting a scalar $\theta$ and finds the tiles that has the maximum sum.
E.g., for $\theta = 6$ the matrix in Table~\ref{tab:toy-rank} becomes the matrix in Table~\ref{tab:toy-transform}.
Observe that, in Table~\ref{tab:toy-transform}, objects that are in the same cluster have negative rank values for each other.
Since the computational complexity of finding the interesting tiles is high, Le Van et al.~\cite{levan} proposes an approximation.

\setlength\tabcolsep{5pt}
\begin{table}[tb]
	\scriptsize
	\centering
		\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
				& $\vo_{1}$ & $\vo_{2}$ & $\vo_{3}$ & $\vo_{4}$ & $\vo_{5}$ & $\vo_{6}$ & $\vo_{7}$ & $\vo_{8}$ & $\vo_{9}$ & $\vo_{10}$ & $\vo_{11}$ & $\vo_{12}$ \\ \hline
			$\vo_{1}$ & -6&-1&-3&-5&2&1&-2&-4&3&0&4&5 \\ \hline
			$\vo_{2}$ & -2&-6&-4&-3&2&1&-5&-1&3&0&4&5 \\ \hline
			$\vo_{3}$ & -2&-4&-6&-3&2&1&-5&-1&3&0&4&5 \\ \hline
			$\vo_{4}$ & -5&-1&-3&-6&2&1&-2&-4&3&0&4&5 \\ \hline
			$\vo_{5}$ & 4&0&2&3&-6&-4&1&5&-5&-3&-2&-1 \\ \hline
			$\vo_{6}$ & 4&0&2&3&-5&-6&1&5&-4&-3&-2&-1 \\ \hline
			$\vo_{7}$ & -2&-4&-5&-3&2&1&-6&-1&3&0&4&5 \\ \hline
			$\vo_{8}$ & -5&-1&-3&-4&2&1&-2&-6&3&0&4&5 \\ \hline
			$\vo_{9}$ & 4&0&2&3&-5&-4&1&5&-6&-3&-2&-1 \\ \hline
			$\vo_{10}$ &4&0&2&3&-4&-5&1&5&-3&-6&-2&-1 \\ \hline
			$\vo_{11}$ &4&0&2&3&-3&-2&1&5&-4&-1&-6&-5 \\ \hline
			$\vo_{12}$ &4&0&2&3&-3&-2&1&5&-4&-1&-5&-6 \\ \hline
		\end{tabular}
	\caption{Transformed matrix}
	\label{tab:toy-transform}
\end{table}

When we apply the tiling algorithm of Le Van et al. to our problem setting,
two fundamental issues arise:
\begin{enumerate}
	\item It finds tiles with a maximum sum, however, cluster structures in the data form minimal tiles in the ranked matrix.
	\item Even though it uses a heuristic, it is still computationally expensive.
\end{enumerate}

First issue is easy to address: instead of finding the maximal tiles as proposed, we mine the minimal ones.
The computational complexity of mining the tiles is high even for the standards of the computationally expensive subspace clustering algorithms.
We address the complexity problem by exploiting the special properties of our problem setting as we explain in the following sections.



%Note that, although the matrix looks very \emph{organized} for the toy dataset,
%this is rarely the case in real world scenarios.
%To find the interesting tiles, computationally expensive tile mining algorithms are required.

\subsection{Subspace Clustering}
\label{sec:rank-subspace}
Because of the effects of so called \emph{curse of dimensionality},
similarities become less meaningful in high-dimensional spaces~\cite{curse}.
Subspace clustering methods tackle this problem by defining a cluster as a pair of an object set and the dimensions in which they are similar.

Searching for cluster structures in every combination of dimensions is not feasible.
To prune the search space, some well known methods~\cite{SUBCLU,FIRES,cartification,clon} in the literature exploit the \emph{apriori principle},
which states that if a set of objects form a cluster in a combination of dimensions,
then the same objects form cluster structures in the subsets of these dimensions.
Following the common practice in the literature~\cite{SUBCLU,clon},
we start by finding the clusters in 1 dimensional projections and iteratively refine them to find the clusters in higher dimensions.

Computational requirement of finding tiles in multiple ranked matrices is multiple of finding tiles in only one matrix.
However, when the ranked matrix is sorted according to the values of the dimension,
it gains some intrinsic properties.

\begin{definition}[Sorted Ranked Neighborhood Matrix]
	If the \emph{ranked neighborhood matrix} is created for a uni-variate data, i.e. one-dimensional data,
	then the columns and rows of the matrix can be sorted to represent the order in the original dimension.
	This matrix is called \emph{sorted ranked neighborhood matrix}.
\end{definition}

For example, if we sort the rows and columns of the ranked matrix in Table~\ref{tab:toy-rank} according to the object values in $A_1$, then we will have the matrix in Table~\ref{tab:toy-transform-ordered}.

\setlength\tabcolsep{5pt}
\begin{table}[tb]
	\scriptsize
	\centering
		\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
			&$\vo_{12}$& $\vo_{11}$& $\vo_{9}$& $\vo_{5}$& $\vo_{6}$& $\vo_{10}$& $\vo_{2}$& $\vo_{7}$& $\vo_{3}$& $\vo_{4}$& $\vo_{1}$& $\vo_{8}$ \\ \hline
			$\vo_{12}$& 0&1&2&3&4&5&6&7&8&9&10&11 \\ \hline
			$\vo_{11}$& 1&0&2&3&4&5&6&7&8&9&10&11 \\ \hline
			$\vo_{9}$&  5&4&0&1&2&3&6&7&8&9&10&11 \\ \hline
			$\vo_{5}$&  5&4&1&0&2&3&6&7&8&9&10&11 \\ \hline
			$\vo_{6}$&  5&4&2&1&0&3&6&7&8&9&10&11 \\ \hline
			$\vo_{10}$& 5&4&3&2&1&0&6&7&8&9&10&11 \\ \hline
			$\vo_{2}$&  11&10&9&8&7&6&0&1&2&3&4&5 \\ \hline
			$\vo_{7}$&  11&10&9&8&7&6&2&0&1&3&4&5 \\ \hline
			$\vo_{3}$&  11&10&9&8&7&6&2&1&0&3&4&5 \\ \hline
			$\vo_{4}$&  11&10&9&8&7&6&5&4&3&0&1&2 \\ \hline
			$\vo_{1}$&  11&10&9&8&7&6&5&4&3&1&0&2 \\ \hline
			$\vo_{8}$&  11&10&9&8&7&6&5&4&3&2&1&0 \\ \hline
		\end{tabular}
		\caption{Ordered ranked neighborhood matrix for $A_{1}$}
	\label{tab:toy-transform-ordered}
\end{table}


The following properties hold for a \emph{sorted ranked matrix} $\mM$,
where $\mM_{ij}$ stands for the value in $i^{th}$ row and $j^{th}$ column:

\begin{property}
	\label{pro:diagonal}
	Minimum values are always on the diagonal.
\end{property}
\begin{proof}
	The diagonals are always zero since the order of rows and columns are the same and each point is most similar to itself.
\end{proof}
\begin{property}
	\label{pro:row}
	In a row, minimum values are next to each other and this neighborhood involves the diagonal.
	For the values on $i^{th}$ row, the following statement holds:
\[
i<j<k \implies \mM_{ij} < \mM_{ik} \wedge j<k<i \implies \mM_{ij} > \mM_{ik}
\]
\begin{proof}
	Since $\mM$ is a ranked neighborhood matrix, if the columns stand for the objects $\vo_{a_0}, \vo_{a_1}, \cdots, \vo_{a_n}$, then $i < j \implies \vo_{a_i} <= \vo_{a_j}$.
	Consequently, for $ \vo_{a_i} < \vo_{a_j} < \vo_{a_k}$ and $\vo_{a_x} \in \DB$,
	\[
	|\{\vo_{a_x}| \dist(\vo_{a_i}, \vo_{a_x}) < \dist(\vo_{a_i}, \vo_{a_j})  \}| < 
	|\{\vo_{a_x}| \dist(\vo_{a_i}, \vo_{a_x}) < \dist(\vo_{a_i}, \vo_{a_k})  \}| 
	\]
	and therefore $i < j < k \implies \mM_{ij} < \mM_{ik}$. Second half of the property can be proven in a similar way.
\end{proof}
\end{property}
\begin{property}
	\label{pro:col}
	In a column, minimum values are next to each other and this neighborhood involves the diagonal.
	Formally, 
\[
i<j<k \implies \mM_{ji} \le \mM_{ki} \wedge j<k<i \implies \mM_{ji} \ge \mM_{ki}
\]
\end{property}
\begin{proof}
	Similar to the proof of Property~\ref{pro:row}, $i<j<k \implies \vo_{a_i} < \vo_{a_j} < \vo_{a_k} \implies \mM_{ji} \le M_{ki}$.
\end{proof}
\begin{property}
	\label{pro:cont}
	Minimal tiles are contiguous.
\end{property}
\begin{proof}
	From the Properties~\ref{pro:row} and~\ref{pro:col}.
\end{proof}



