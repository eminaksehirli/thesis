\begin{landscape}
	\begin{figure}[t]
		\begin{centering}
			\includegraphics[width=1.5\textwidth,keepaspectratio=true,draft=false]{figures/summary}
			\caption{\eclat and \bigfim on \mapreduce framework}
			\label{fig:mapreduce}
		\end{centering}
	\end{figure}
\end{landscape}

\section{Frequent Itemset Mining on \mapreduce}
\label{sec:dis-fim}

%Small intro
We propose two new methods for mining frequent itemsets in parallel on the \mapreduce framework that can handle low frequency thresholds.
Our first method, called \declat, is a pure \eclat method that distributes the search space as evenly as possible among mappers.
This technique is able to mine large datasets, but can be prohibitive when dealing with massive amounts of data.
Therefore, we introduce a second, hybrid method that first uses an \apriori based method to extract frequent itemsets of length $k$ and later on switches to \eclat when the projected databases fit in memory.
We call this algorithm \bigfim.


\subsection{\declat}
\label{ssec:declat}

%Introduction of our method and current techniques for partitioning
Our first method is a distributed version of \eclat that partitions the search space more evenly among different processing units.
Current techniques often distribute the workload by partitioning the transaction database into equally sized sub data\-bases, also called shards, e.g., the Partition algorithm~\cite{ref:savasere1995}.
Each of the sub databases can be mined separately and the results can then be combined.
However, all local frequent itemsets should be combined and counted again to prune the globally infrequent ones, which is expensive.
%The work is however not yet finished, since many itemsets might have not been counted in some of the shards.
%As such, these methods must combine all mined sets, and check which of them should be counted still in the remaining shards.

%Problems with Partition algorithm
First of all, this approach comes with a large communication cost,
i.e., the number of sets to be mined can be very large, moreover, the number of sets that have to be recounted can also be very large.
Implementing such a partitioning technique in \hadoop is therefore prohibitive.
A possible solution for the recounting part, is to mine the sub databases with a lower threshold,
hence, decreasing the number of itemsets that might have been missed.
However, another problem then occurs: indeed, each shard defines a local sub database for which the local structure can be very different from the rest of the data.
As a result, computation of the frequent itemsets can blow up tremendously for some shards,
although many of the sets are actually local structures and far from interesting globally.

%Good thing about our algorithm
We argue that our method does not have to deal with such problems, since we are dividing the search space rather than the data space.
Therefore, no extra communication between mappers is necessary and no checking of overlapping mining results has to be accounted for.
We also claim that \eclat, more specifically \eclat using diffsets~\cite{ref:zaki2003}, memory-wise is the best fit for mining large datasets.

%Why DEclat
First of all, \eclat uses the depth-first approach, such that only a limited number of candidates have to be kept in memory even while finding long frequent patterns.
In contrast, \apriori, the breadth-first approach, has to keep all $k$-sized frequent sets in memory when computing $k$+1-sized candidates.
Secondly, using diffsets limits the memory overhead approximately to the size of the original tid-list root of the current branch~\cite{goethals_survey_2003}.
This is easy to see: a diffset represents the tids that have to be subtracted from the tid-list of the parent to obtain the tid-list for this node, so,
at most the tids that can be subtracted on a complete branch extension is the size of the original tid-list.

%Introduction of our method
\declat operates in a three step approach as shown on the left side of Figure~\ref{fig:mapreduce}.
Each of the steps can be distributed among multiple mappers to maximally benefit from the cluster environment.
For this implementation we do not start with a typical transaction database, rather we utilize immediately the vertical database format.
%We claim that starting from transactional or vertical database format is a preprocessing step, rather than a mining step.

\vspace{0.2cm}
%Step 1, filter singletons
1) \textbf{Finding the Frequent Items:}
During the first step, the vertical database is divided into equally sized blocks (shards) and distributed to available mappers.
Each mapper extracts the frequent singletons from its shard.
In the reduce phase, all frequent items are gathered without further processing.

\vspace{0.2cm}
%Step 2, generate x-\fis
2) \textbf{$k$-\fis Generation:}
In this second step, $\mathcal{P}_{k}$, the set of frequent itemsets of size $k$, is generated.
First, frequent singletons are distributed across $m$ mappers.
Each of the mappers finds the frequent $k$-sized supersets of the items by running \eclat to level $k$.
Finally, a reducer assigns $\mathcal{P}_{k}$ to a new batch of $m$ mappers.
Distribution is done using \aRoundRobin.
%One reducer again gathers all information computed by the mappers, then assigns the $k$-\fis seeds to a new batch of mappers.

\vspace{0.2cm}
%Step 3, mine subtree
3) \textbf{Subtree Mining:}
The last step consists of mining the prefix tree starting at a prefix from the assigned batch using \eclat.
Each mapper can complete this step independently since sub-trees do not require mutual information.


\subsection{\bigfim}
\label{ssec:big-fim}

%Why Big-Fim
Our second method overcomes two problems inherent to \declat.
First, mining for $k$-\fis can already be infeasible.
Indeed, in the worst case, one mapper needs the complete dataset to construct all 2-\fis pairs.
Considering BigData, the tid-list of even a single item may not fit into memory.
Secondly, most of the mappers require the whole dataset in memory in order to mine the sub-trees (cf. Section~\ref{sec:experiments-balancing}).
Therefore the complete dataset has to be communicated to different mappers,
which can be prohibitive for the given network infrastructure.
Flow of \bigfim is shown on the right side of Figure~\ref{fig:mapreduce}.

\vspace{0.2cm}
1) \textbf{Generating $k$-\fis:}
%Working Big-Fim
\bigfim covers the problem of large tid-lists by generating $k$-\fis using the breadth-first method.
This can be achieved by adapting the Word Counting problem for documents~\cite{dean_mapreduce:_2004}, 
i.e., each mapper receives part of the database (a document) and reports the items/itemsets (the words) for which we want to know the support (the count).
A reducer combines all local frequencies and reports only the globally frequent items/itemsets.
These frequent itemsets can be redistributed to all mappers to act as candidates for the next step of breadth-first search.
These steps can be repeated $k$ times, to obtain the set of $k$-\fis.

%Why \apriori for first part
Computing the $k$-\fis in a level-wise fashion is the most logical choice:
we do not have to keep large tid-lists in memory,
rather we need only the itemsets that have to be counted.
In \apriori, for the first few levels, keeping the candidates in memory is still possible---as opposed to continuing this process to greater depths.
Alternatively, when a set of candidates does not fit into memory, partitioning the set of candidates across mappers can resolve the problem.

%\textcolor{red}{somewhere this ref should be included ~\cite{lin_apriori-based_2012}.}

\vspace{0.2cm}
2) \textbf{Finding Potential Extensions:}
%Computing Tidlists
After computing the prefixes, the next step is computing the possible extensions, i.e., obtaining tid-lists for ($k+1$)-\fis.
This can be done similar to Word Counting, however, now instead of local support counts we report the local tid-lists.
A reducer combines the local tid-lists from all mappers to a single global tid-list and assigns complete prefix groups to different mappers.

\vspace{0.2cm}
3) \textbf{Subtree Mining:}
%Mining
Finally, the mappers work on individual prefix groups.
A prefix group defines a conditional database that completely fits into memory.
The mining part then utilizes diffsets to mine the conditional database for frequent itemsets using depth-first search.

\vspace{0.2cm}
%Iterative approach for finding x-\fis
Using 3-\fis as prefixes generally yields well-balanced distributions (cf. Section~\ref{sec:experiments-balancing}).
Unfortunately, when dealing with large datasets, a set of 3-\fis extensions can still be too large to fit into memory.
In such cases we can continue the iterative process until we reach a set of $k$-\fis that are small enough.
To make an estimate on the size of the prefix extensions, the following heuristic can be used.
Given a prefix $p$ with support $s$ and order $r$ out of $n$ items,
the size of the conditional database described by $p$ is at most $s * (n-(r+1))$.
Recall that when a set of candidates does not fit in to memory, the set of candidates can also be distributed and then the diffsets does not incur an exponential blow up in memory usage.

\subsection{Implementation details}
\label{ssec:implementation_details}

In our methods frequent itemsets are mined in step 3 by the mappers and then communicated to the reducer.
To reduce network traffic, we encoded the mined itemsets using a compressed trie string representation for each batch of patterns,
similar to the representation introduced by Zaki~\cite{zaki_efficiently_2005}.
Basically, delimiters indicate if the tree is traversed downwards or upwards, and if a support is specified.
As an example:

%
\vspace{1em}
\begin{tabular}[h]{|m{1.8cm}| m{.4cm} m{5cm}}
%\begin{align*}
\parbox{1.8cm}{
a (300)\\
a b c (100)\\
a b d (200)\\
b d (50)
}
%\end{align*}
&
%is encoded as,
$\rightarrow$
&
$$\text{a(300)$|$b$|$c(100)\$d(200)\$\$\$b$|$d(50)}$$
\end{tabular}
\vspace{1em}

As a second remark, we point out that our algorithm computes a superset of the closed itemsets found in a transaction dataset.
We can easily do so by letting the individual mappers report only the closed sets in their subtree.
Although it is perfectly possible to mine the correct set of closed itemsets, we omitted this post-processing step in our method.
