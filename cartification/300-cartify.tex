\section{Cartification}\label{section:cartification}
In this section we explain our transformation from a high dimensional numeric space into a transaction database.
In particular we show how this transformation preserves neighborhood information by co-occurrence of objects.

\subsection{Basic Notions}
Our transformation turns the neighborhood information of a \textit{high dimensional data space} into a \textit{transaction database}.

\textsc{Input: high dimensional numeric database}\\
Let $\Attributes = \{\A_1, \ldots, \A_d\}$ be a set of $d$ dimensions. A data object, $o$, is defined as a tuple of values over $\Attributes$.
A $d$-dimensional database, $\DB$, is a collection of $n$ data objects such that every object in $\DB$ is represented by a $d$-dimensional vector $\mathbf{o} \in \mathbb{R}^d$.
$o_{i}$ represents the value of $\mathbf{o}$ for the attribute $\A_{i}$.

Usually, similarity is assessed by distance functions such as the Euclidean distance between a pair of objects $\mathbf{o}, \mathbf{p} \in \DB$:
\begin{center}
$dist_{\Attributes}(\mathbf{o}, \mathbf{p}) = \sqrt{\sum_{A_i \in \Attributes} (o_{i} - p_{i})^2}$
\end{center}
The curse of dimensionality~\cite{curse} intuitively states that if the number of dimensions increase, distances between objects grow more and more alike. Formally:
\[ \lim_{d \to \infty} \frac{\max\limits_{\mathbf{p} \in \DB}
\dist_{\Attributes}(\mathbf{o}, \mathbf{p}) - \min\limits_{\mathbf{p} \in \DB} \dist_{\Attributes}(\mathbf{o}, \mathbf{p})}{\min\limits_{\mathbf{p} \in \DB} \dist_{\Attributes}(\mathbf{o}, \mathbf{p})} = 0 \]
Thus, local neighborhood becomes meaningless for a large number of dimensions. Our transformation tries to preserve the local neighborhoods inside clusters that show high similarity. Therefore, we use multiple views on the data by simply using different similarity measures for each view.

We use the notation of $\M$ as a set of different measures to induce all of these views.
An individual measure $m$ is simply a function that represents the similarity relation between pairs of objects.
For example, we use $m_S$ for a measure over arbitrary attribute set $S \subseteq \Attributes$, i.e., $m_{\Attributes}(\mathbf{o}, \mathbf{p})$ represents the similarity of objects $\mathbf{o}$ and $\mathbf{p}$ over all attributes.
Since we exploit the relative similarities of objects, a measure can be a dissimilarity measure (e.g. Euclidean distance),
or a similarity measure (e.g. Jaccard similarity), as long as the objects can be ordered by their pairwise similarity.
%an ordered relation of objects is possible. %We elaborate the details in Section~\ref{section:formal-definition}.
Intuitively, one can think of $m$ as the projection scheme, which, obviously, can model any arbitrary transformation of the data.

\vspace{0.5em}\textsc{Output: transaction data representation}\\
For a set of items $\mathcal{I}$, an \emph{itemset} $\objset$ is defined as $\objset \subseteq \mathcal{I}$.
A transaction $t = (\mathit{tid}, \objset)$ is a pair of unique transaction id and an itemset.
% set of items $I \subseteq \mathcal{I}$.
A transaction database $\TDB$ is a set of transactions over $\mathcal{I}$.
The \emph{support} of an itemset $\objset$ in database $\TDB$ is the number of transactions in $\TDB$ in which $\objset$ occurs, i.e.,
\[
\supp_\TDB(\objset) = |\{t \in \TDB \mid \objset \textrm{ occurs in }t\}|.
\]
Support satisfies the monotonicity property~\cite{agrawal93}, i.e.
\[
Y \subseteq \objset \Rightarrow \supp(Y) \geq \supp(\objset),
\]
An itemset is called \emph{frequent} if its support is
larger than some user-defined threshold called the \emph{minimum support} or {\minsup} for short.

A cartification database $\CB$ is a special type of transactional data\-base that has ordered transactions. Cartification is explained in detail in the following section.



\subsection{Formal Transformation}\label{section:formal-definition}

Cartification transforms the original relational dataset into a coalition of different views on the data, such that each of them contributes with the neighborhood information of a given similarity measure.

%For each object $\mathbf{o}$, we create a cart, $\cart$, that is a tuple of its neighbors, $\mathbf{p}_i \in \DB$, ordered descending by their similarity to $\mathbf{o}$.

\begin{definition}[View of an object]
\label{def:view-of-object}
	Let $\mathbf{o}, \mathbf{p}_{i} \in \DB$,
$|\DB| = n$ the number of objects in $\DB$, and $m$ a dissimilarity measure.
The cart $\cart_m(\mathbf{o})$ of object $\mathbf{o}$ is a tuple of its neighbors, ordered descending by their similarity to $\mathbf{o}$. Formally,
\begin{eqnarray}
	\cart_{m}(\mathbf{o})  =  (\mathbf{p}_{1}, \mathbf{p}_{2}, \cdots, \mathbf{p}_{n}) 	
\nonumber
\end{eqnarray}
provided that,
\begin{equation*}
	m(\mathbf{o}, \mathbf{p}_{i}) \le m( \mathbf{o}, \mathbf{p}_{j}) \iff i < j \quad .
\end{equation*}
\end{definition}

Neighborhood information on objects $\mathbf{p}$ that are far away from $\mathbf{o}$, contributes very little towards the discernibility of local cluster structures, therefore, far away neighbors can be excluded from the carts.
That is, carts can be trimmed to include just the first $k$  elements. Note that the first $k$ elements of the cart represent the \emph{top-$k$ Nearest Neighbors} (\emph{kNN}) of object $\mathbf{o}$ w.r.t\ a similarity measure $m$.
Hence, a cart reflects the local neighborhood of a single object by using the perspective of the given measure $m$. It provides a specific view on the data. Intuitively, one can think of a projection of the database with $m$ as the projection scheme. Obviously, $m$ can model any arbitrary transformation of the data. This is used to capture one specific view on the neighborhood of each object. For the entire database we model the view of a measure as follows.


\begin{definition}[View of a measure]
A local view $\CB_{m}$ is a reflection of neighborhood information from the view of a single similarity measure $m$:
\begin{eqnarray}
	\CB_{m}  =  \bigcup_{\mathbf{o} \in \DB} \cart_{m}(\mathbf{o})
%	\label{eqn:cart-measure}
\nonumber
\end{eqnarray}
\end{definition}

Using the transformation of only one measure would be too restrictive, as it is the case with the single projection of typical dimensionality reduction techniques~\cite{PCA,NonlinearBook} or feature selection methods~\cite{feature_selection1,feature_selection2}. In contrast to this, we employ multiple views to collect information about neighborhoods. Intuitively, we use multiple projections instead of a single one. However, again this can be more general space transformations, depending on the used measures $\M$.

\begin{definition}[Transformed database]
Let $\M$ be the set of measures, and $\CB_{m}$ a {\em local view} of the similarity measure $m$, then a cartified database $\CB$ is defined as
\begin{eqnarray}
	\CB = \bigcup_{m \in \M}\CB_{m}
%	\label{eqn:cart-top}
\nonumber
\end{eqnarray}
\end{definition}

After the transformation, neighbors of each object for each measure become clearly visible on their corresponding carts. Aggregating this neighborhood information from a selection of measures will reveal the cluster structures.
We utilize the intrinsic property of neighborhoods, that is, the strong dependence of objects with high similarity inside a cluster is reflected in their co-occurrence in both their own neighborhoods, as well as in the neighborhoods of objects close-by.



\subsection{Co-occurrence of objects}\label{section:co-occurrence}
Frequent occurrence of an object set $\objset$ in $\CB$ indicates that these objects are often spotted in the same neighborhood together in the original space. That is, they are likely to be related.
Furthermore, this effect is added to by co-occurrence in different views.
Every measure reflects a different set of relations in the data and cartification extracts the neighborhood information for each similarity measure separately. As such, extra measures add information;  the accuracy of individual measures is not disturbed by irrelevant views.

\begin{definition}[co-occurring object set]
\label{def:occurrence}
The $\occurrence$ of an object set $\objset$ is the number of carts $\cart \in \CB$ that are a superset of $\objset$. Formally,
\begin{equation}
	\occurrence(\objset) = |\{ \cart \given \objset \subset \cart, \cart \in \CB \}|
\nonumber
\end{equation}
	A cluster $\objset$ is then defined as a set of co-occurring objects w.r.t.\ parameter $\minsup$:
	\[\occurrence(\objset) \geq \minsup\]
\end{definition}


We measure the occurrence of an object set $\objset$ by simply counting the co-oc\-cur\-ren\-ces of these objects in any cart in our cartified database $\CB$. This can be considered as an implicit similarity assessment on the objects in $\objset$. Clustered objects have to share a large amount of objects in their respective neighborhoods. Hence they show high mutual similarity to each other. Please note that this clustering criterion can be considered as a generalization of Shared Nearest Neighborhood ({SNN}) clustering~\cite{Jarvis73-SNNClustering}.

Essentially, {SNN} is a simplified version of our co-occurrence analysis.
In contrast to SNN, we use multiple local views, which in turn better grasp the structures that exist only in data projections. Furthermore, we keep the order of the neighbors, that can be used to improve the following analysis steps after our data transformation,
e.g., by interestingness measures based on frequency or locality within carts. SNN simply uses the neighborhood of objects in the full space as a single view on the data.
Thus, SNN can be written as a specialized version of our transformation:
\[	\mathit{SNN}(\mathbf{o},\mathbf{p}) = | \cart(\mathbf{o}) \cap \cart(\mathbf{p})|\]
According to SNN-clustering~\cite{Jarvis73-SNNClustering}, objects in a cluster has high mutual similarity $\mathit{SNN}(o,p) \geq k_t$, i.e., each object pair exceeds a similarity threshold $k_t$. In our generalized notion such a cluster is detected as a co-occurring object set $\objset$ with \[\occurrence(\objset) \geq \minsup = k_t \quad .\] Given this mapping to SNN~\cite{Jarvis73-SNNClustering}, we can detect all clusters according to this cluster definition. However, we are more general than SNN-clustering. We overcome limitations of SNN. Since we use multiple views, we are not limited to the full space using $m_{\Attributes}(\mathbf{o}, \mathbf{p})$ as the only measure. Our transformation, on the other hand, can exploit lower dimensional neighborhood information and so circumvent the curse of dimensionality of considering all dimensions at once.

\vspace{0.5em}\textsc{Example:} Suppose a high dimensional database having a set of objects $\objset$ form a cluster in dimensions $A_1,A_2,A_3$ but not in dimensions $A_4,A_5,A_6$. 
The hidden cluster will become a frequent pattern in the views from the measures that capture the data distributions in the first three dimensions.
Although attributes $A_4,A_5,A_6$ do not support this cluster, e.g., show scattered data distribution for the object set $\objset$, they will not affect the support from $A_1,A_2,A_3$, therefore the cluster will remain detectable.
On the contrary, in the original space, taking all the dimensions into account would distort the distances, obstructing the detection of the cluster.

\vspace{0.5em}
\textsc{Formally:}\\ Clustered objects $\objset = \{\mathbf{p}_1, \ldots , \mathbf{p}_n\}$ will be clearly separated from residual objects $\mathbf{q} \not \in \DB \setminus \objset$ in at least some dissimilarity measures of $\M$:

\vspace{0.5em}\textsc{Cartified Space:}
\begin{align*}
\occurrence(\objset) \geq \minsup  &  \stackrel{\mbox{(Def.~\ref{def:occurrence})}}{\Longleftrightarrow} 
\exists V \subseteq \DB \times \M \wedge |V| \geq \minsup \\
\forall (\mathbf{o},m) \in V : \objset \subset \cart_{m}(\mathbf{o}) & \stackrel{\mbox{(Def.~\ref{def:view-of-object})}}{\Longleftrightarrow}
\exists V \subseteq \DB \times \M \wedge |V| \geq \minsup \\
\forall (\mathbf{o},m) \in V : m(\mathbf{o},& \mathbf{p}_j) < m(\mathbf{o},\mathbf{q}) \quad
\forall \mathbf{p}_j \in \objset \wedge \mathbf{q} \not \in \cart_{m}(\mathbf{o})
\end{align*}

Thus, there exists a set of carts that reflect the similarity of all clustered objects $\objset$ in some of the views $\M$. The similarity of a cluster is preserved in these views, i.e., objects $\mathbf{p}_j$ are clustered together in the neighborhood of a central object $\mathbf{o}$, while other objects $\mathbf{q}$ are clearly separated from this central object. 
Please note that there might be other contradicting views, however, these not have a negative effect on the number of co-occurrences of $\objset$. Therefore, similarity and neighborhoods are preserved. In contrast to this, the original data space is effected by irrelevant dimensions, and thus,  does not reveal any cluster structures.

\vspace{0.5em}\textsc{Original Space:}
\[\forall \mathbf{o},\mathbf{p} \in \objset \wedge \forall \mathbf{q} \not\in \objset ~:~
m_{\Attributes}(\mathbf{o}, \mathbf{p}) \approx m_{\Attributes}(\mathbf{o}, \mathbf{q})
\]
Due to the curse of dimensionality~\cite{curse,Fayyad99-NN-curse}, all objects have similar distances to each other, thus, no cluster structure can be observed.


\subsection{Instantiation: One-Dimensional Data Projections}
\label{section:co-occurrenc}

In order to preserve neighborhoods and circumvent the curse of dimensionality~\cite{curse,Fayyad99-NN-curse}, we follow the idea of lower dimensional projections as meaningful instantiation of different measures. Lower dimensional projections have shown to be effective for query processing~\cite{Keim00-NN-Projections} and subspaces clustering~\cite{parsons2004survey} in high dimensional data\-bases. They capture neighborhood information w.r.t.\ different views on the data and provide us the required similarity measures on the database:

\begin{definition}[A Projection Measure]\label{def:projectionMeasure}\-\\
Given a set of numeric dimensions $S \subseteq \Attributes$, we use the projection of $\DB$ specified by the following measure:
\[
m_S(\mathbf{o},\mathbf{p}) =
dist_{S}(\mathbf{o}, \mathbf{p}) =
\sqrt{\sum_{A_i \in S} (o_{i} - p_{i})^2} \]
\end{definition}

W.l.o.g.\ we use Euclidean distance as dissimilarity measure, however, any other distance restricted to a subset of attributes will work here as well. Using this definition of projection measures, the set of all measures $\M$ is simply a collection of different projections of the data. Choosing this set of projections can be instantiated by correlation analysis or subspace search methods~\cite{ENCLUS,RIS,HICS}. 
%However, this selection problem has exponential complexity~\cite{CLIQUE}.

However, we will show that such prior analysis and exponential overhead is not required in our case. As minimum requirement the similarity measures have to identify neighborhoods for each individual dimension only. Then, possible correlation in higher-dimensional projections is assessed by our co-occurrence.
%and can be used in a later step to refine the initial set of measures (cf.~Section~\ref{section:application}).

We simply use $\M = \{m_{A_1},\ldots,m_{A_d}\}$ as initial set of measures. Each measure $m_{A_i}$ represents a one-dimensional projection of the data w.r.t.\ attribute $A_i$. It is simply computed by the Euclidean distance in this dimension. Using these measures, we allow for similarity assessment in any projection of the data. A co-occurring object set $\objset$ can show mutual similarity in an arbitrary subspace $S \subseteq \Attributes$:
\[\exists A_i \in S ~:~ \objset \subset \cart_{m_i}(\mathbf{o}) \quad \forall \mathbf{o} \in \objset  \quad \Longleftrightarrow
 | o_i - p_i | < | o_i - q_i | \quad \forall \mathbf{o},\mathbf{p} \in \objset \wedge \mathbf{q} \not \in \objset \]



\begin{figure}[t]
\centering
\begin{tikzpicture}[only marks,x=0.25cm,y=0.25cm]
\def\xmin{0}
\def\xmax{23.9}
\def\ymin{0}
\def\ymax{23.9}
\draw[style=help lines, dashed, ultra thin, ystep=2, xstep=2] (\xmin,\ymin) grid (\xmax,\ymax);
\draw[->] (\xmin,\ymin) -- (\xmax,\ymin) node[right] {$x$};
\draw[->] (\xmin,\ymin) -- (\xmin,\ymax) node[above] {$y$};
\foreach \x in {2,4,...,22}    \node at (\x, \ymin) [below] {\x};
\foreach \y in {2,4,...,22}    \node at (\xmin,\y) [left] {\y};
\draw plot[mark=ball,mark size=2.5pt]  coordinates {(2,3)(4,2)(5,5)(7,8)(8,6)(12,12)(16,17)(18,16)(19,19)
(21,22)(22,20)};
\draw node at (2.8,3) {1};
\draw node at (4.8,2) {2};
\draw node at (5.8,5) {3};
\draw node at (7.8,8) {4};
\draw node at (8.8,6) {5};
\draw node at (12.8,12) {6};
\draw node at (16.8,17) {7};
\draw node at (18.8,16) {8};
\draw node at (19.8,19) {9};
\draw node at (22,22) {10};
\draw node at (23,20) {11};
\end{tikzpicture}
\caption{Example dataset}
\label{fig:sampledata}
\end{figure}

\begin{table}[tb]
\begin{center}
\begin{tabular}[t]{c c@{\hspace{2.5em}}c c}
\toprule
{$x$} & \textbf{cart} & $y$ & \textbf{cart} \\
\midrule
$1$ & $ \{1,2,3\}$ & 1 & $ \{1,2,3\}$ \\
$2$ & $ \{1,2,3\}$ & 2 & $ \{1,2,3\}$ \\
$3$ & $ \{2,3,4\}$ & 3 & $ \{1,3,5\}$ \\
$4$ & $ \{3,4,5\}$ & 4 & $ \{3,4,5\}$ \\
$5$ & $ \{3,4,5\}$ & 5 & $ \{3,4,5\}$ \\
$6$ & $ \{5,6,7\}$ & 6 & $ \{4,6,8\}$ \\
$7$ & $ \{7,8,9\}$ & 7 & $ \{7,8,9\}$ \\
$8$ & $ \{7,8,9\}$ & 8 & $ \{7,8,9\}$ \\
$9$ & $ \{8,9,10\}$ & 9 & $ \{7,9,11\}$ \\
$10$ & $ \{9,10,11\}$ & 10 & $ \{9,10,11\}$ \\
$11$ & $ \{9,10,11\}$ & 11 & $ \{9,10,11\}$ \\
\bottomrule
\end{tabular}
\caption{Cartification of the data in Figure~\ref{fig:sampledata}, for $k=3$.}
\label{tbl:samplecart}
\end{center}
%\vspace{-1em}
\end{table}%

\begin{figure}
\centering
\begin{tikzpicture}[x=0.5cm,y=0.3cm]
\def\xmin{0}
\def\xmax{11.8}
\def\ymin{0}
\def\ymax{10.8}
\draw (\xmin,\ymin) -- (\xmax,\ymin);% node[right] {$x$};
\draw[->] (\xmin,\ymin) -- (\xmin,\ymax);% node[above] {$y$};
\foreach \x in {1,2,...,11}    \node at (\x, \ymin) [below] {\x};
\foreach \y/\ytext in {0.6/2,1.2/4,1.8/6,2.4/8,3/10}    \draw (1pt, \y cm) -- (-3pt, \y cm) node[anchor=east] {$
\ytext$};
\node[left=0.7cm,rotate=90] at (0,8) {Frequency};
\draw[ycomb, color=blue,line width=0.3cm] plot coordinates{(1,5) (2,5) (3,10) (4,6) (5,6) (6,2) (7,6) (8,6)
(9,10) (10,5) (11,5)};
\end{tikzpicture}
\caption{Item Frequencies for $k=3$}
\label{fig:samplefreqs}
%\vspace{-1em}
\end{figure}

Please note that we might consider more complex similarity measures as alternative to the current one-dimensional instantiation, e.g. by using correlation analysis or subspace search \cite{ENCLUS, HICS, CMI}. Our current solution abstracts from such instantiations and provides the general processing for any set of similarity measures $\M$. 

Let us now give an example of how we can identify central elements of a cluster. Assume we are given the two-dimensional dataset with $\Attributes = \{x,y\}$ and data objects as shown in Figure~\ref{fig:sampledata}.
For this example, as a cartification strategy we use $k$-nearest neighbors on separate dimensions to create carts. Formally, inverse of Euclidean distance on separate dimensions are our similarity measures,
\begin{align*}
	m_{\{x\}}(\mathbf{o}, \mathbf{p}) = | o_{x} - p_{x} | \\
	m_{\{y\}}(\mathbf{o}, \mathbf{p}) = | o_{y} - p_{y} |
\end{align*}

where $o_{x}$ and $o_{y}$ are the $x$ and $y$ attribute values of the data object $\mathbf{o}$, respectively.
Using these measures, the \textsc{Cartify} algorithm (cf.~ Section~\ref{sec:algo}) creates a database where each cart
is composed of the items ordered by one dimensional distances to each item.

On this toy example, essential information regarding the formation of items can be extracted with a rather simple method. Instead of taking into account detailed ordering in the carts, we truncate each transaction to the same size, $k$, disregard the inner order, and treat each transaction as a set.

Note that, this process converts the cartified database to a regular transactional database where each transaction is the $k$-nearest neighbors of each item on each dimension.



There is only one parameter needed for the transformation:
%selection of the
the truncation threshold $k$, i.e.,
%is important.
%We first need to choose a threshold $k$,
the number of the $k$ nearest neighbors that will be added to each cart.
For example, the resulting database for $k=3$ is shown in Table~\ref{tbl:samplecart}. The first two columns show the cartification for the $x$-dimension, and the second two columns for the $y$-dimension.
Every row corresponds to the cart generated from one of the data points.
For instance, for point $3$, the three nearest neighbors in the $x$-dimension are points $2, 3$, and $4$, while for the $y$-dimension these are $1, 3$, and $5$.

In Figure~\ref{fig:samplefreqs}, we plot the frequencies of all items (points) in this cartified database.
The plot shows that are two points, $3$ and $9$, have high frequencies.
As Figure~\ref{fig:sampledata} shows, these points are in the centers of the clusters  $\{1,2,3,4,5\}$ and $\{7,8,9,10,11\}$ respectively.  Additionally,  point $6$ has a very low frequency which corresponds to the point being an outlier w.r.t. all other points in the figure.


\subsection{Algorithm and Complexity Analysis}
\label{sec:algo}

With the above, we can now formally introduce the \textsc{Cartify} algorithm.
Suppose we are given a database $\DB$ over attributes $\Attributes$ and a set of similarity measures $\mathcal{M}$, defined over  $S \subseteq \Attributes$ as given in Definition~\ref{def:projectionMeasure}.
The pseudo-code is given as Algorithm~\ref{algo:cartify}.

\begin{algorithm}[t]
\begin{algorithmic}[1]
	\REQUIRE A database $\DB$, set of similarity measures $\mathcal{M}$
	\ENSURE The cartified database $\CB$ of $\DB$ over $\mathcal{M}$
	\STATE $\CB \assign \emptyset$
	\FORALL{$m \in \mathcal{M}$}
		\STATE $\CB_{m} \assign \emptyset$
		\FORALL{$\mathbf{o} \in \DB$}
			\STATE $\cart \assign$ sort $\mathbf{p} \in \DB$ according to $m(\mathbf{o}, \mathbf{p})$
			\STATE $\CB_{m} \assign \CB_{m} \cup \cart$
		\ENDFOR
		\STATE $\CB \assign \CB \cup \CB_{m}$

	\ENDFOR
	\RETURN $\CB$
\end{algorithmic}
\caption{The \textsc{Cartify} Algorithm}
\label{algo:cartify}
\end{algorithm}


For each similarity measure (line~2) and for each object $\mathbf{o}$ in $\DB$ (line~4) a cart $\cart$ is created. Each cart is an ordered list of the objects in $\DB$, sorted descending by their similarity to $\mathbf{o}$.
The collection of all carts together forms the final cartified database, $\CB$ (line~8). During the cartification process, local views for each measure $\CB_{m_i}$ are kept as well (line~6).

Theoretically, space requirement of \textsc{Cartify} algorithm is $n \times n \times |\mathcal{M}|$, where $n$ is the number of items and $|\mathcal{M}|$ is the number of measures.
However, for practical applications, it is not required to cartify the whole database. Instead, carts can be trimmed to a constant value to limit the size of the database.

Worst case time complexity of \textsc{Cartify} is $\bigO{n^{2}\log(n)|\mathcal{M}|}$. For some measures, e.g., 1D projections, sorting can be done per measure, in the outer loop, which results in a time complexity of $\bigO{n\log(n)|\mathcal{M}|}$.
Moreover the processing of the cartification can easily be parallelized over shared memory or distributed systems.
