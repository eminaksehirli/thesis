\chapter{Introduction}
\label{ch:introduction}
%\chapterprecishere{Through every rift of discovery some seeming anomaly drops out of the darkness, and falls, as a golden link into the great chain of order.{\\[0.5cm]{\-\hfill --- Edwin Hubbell Chapin}}}
\vspace{10em}
}

For quite a few years now, data became more influential then it has ever been.
Considering how it shapes the world around us, be it more effective decisions or the services that became part of our lives,
one can see that the importance of data will not fade away soon.


Organizations around the world have already understood the importance of data and for the last few years, they have been trying to keep all the information they can access.
Almost none of the organizations that has a future plan spanning more than 5 years can ignore the importance of data:
commercial companies, governmental organizations, not-for-profit organizations, and even political candidates.
Of course, this new trend of data-oriented approaches have been going hand in hand with the advancements in data acquisition and storage techniques in the recent years.

Technological advancements in data acquisition was so fast and the prices for data storage dropped so quickly that
people started to collect data without considering how to structure it or even whether it will be useful.
We can see the effects of this \emph{data hoarding}
as the technologies that have recently become very popular:
while NoSQL and column-based databases enable collection of unstructured data,
simple parallelization platforms, such as MapReduce, provide tools for wrangling the data.
All of these trends are addressed by one umbrella term: \emph{BigData}.


Since it is not trivial to annotate the massive amounts of data, unsupervised methods are a good fit for BigData analysis.
While cluster analysis can be used as a pre-processing step to summarize and prepare data for more elaborate supervised data analysis techniques,
frequent pattern mining methods can be used to sift through data to discover interesting relations.

One of the characteristics of the BigData is, rather unsurprisingly, the amount of data,
and more precisely, the amount of data that are associated with a single data object.
Although, intuitively, more data bring better and more accurate understandings,
it is also possible that redundancy in data just hinder exploration of interesting relations.
Moreover, it has been mathematically shown that when the number of dimensions increase,
data objects become more and more similar to each other.
This phenomenon is known as \emph{the curse of dimensionality}.

Because the notion of similarity is distorted in higher dimensional spaces,
traditional clustering methods, which heavily depend on similarity measures, cannot be as effective.
On the other hand,
%Although similarity between high dimensional objects is not meaningful,
similarity according to subsets of attributes is still meaningful and can be used to discover interesting relations.

\textit{For example, nowadays a customer can be associated with credit ratings, shopping habits, travel patterns, entertainment choices, sport habits, etc. Even medical conditions or other private information might be available due to the data integration with a multitude of data sources. Considering all the aforementioned data, each customer becomes very unique and almost equally dissimilar to any other customer. This makes the notion of ``similar customers'' meaningless. Nevertheless, a meaningful customer segmentation can be achieved by looking at individual dimensions, e.g.\ travel and sport habits only. In both of these individual attributes customers might be clustered. Even in the combination of these two attributes, the same customer structure might be detected as clusters.}

Subspace clustering algorithms tackle this high dimensionality problem by defining a cluster as a pair of two sets:
the objects that are similar to each other and the dimensions in which they are similar.
Consequently, limiting the measurement space degrades the effects of curse of dimensionality,
and highlights the similarities of objects by producing more compact clusters.
However, dimension search makes the already NP-hard problem of cluster analysis even more complicated.


In this thesis, we approach the high dimensionality problem from a neigh\-bor\-hood-oriented point of view.
We investigate various ways to transform a relational database, so that we can expand our tool set that we can use to extract knowledge.
In a nutshell, \emph{cartification} transformation enables the utilization of frequent pattern mining methods on relational databases
while neighborhood-based representation opens new possibilities for visualization and interactiveness.

The thesis is organized as follows:

\textbf{Chapter~\ref{ch:cartification}} introduces the main theme of this thesis: \emph{Cartification} transformation.
Cartification transforms a relational database into a transaction database 
by preserving the localities of individual data objects as neighborhoods.
By transforming the data space, cartification enables the whole domain of frequent itemset mining (FIM) methods to be used for relational data analysis.
We show that the \emph{curse of dimensionality} can be diminished by using localities
and applying even the most basic FIM methods on cartified data produces results that beat the state of the art cluster analysis methods.


\textbf{Chapter~\ref{ch:clon}} introduces \clon algorithm.
Cartification is a general transformation that can be applied on any type of data
as long as a similarity measure between data objects are defined.
When a total order of the objects is possible, i.e., the attribute under consideration is univariate,
then the transformed database gains some intrinsic properties.
These properties can be exploited to eliminate the computationally expensive process of frequent itemset mining.
\clon is orders of magnitude faster than the original cartification and gives comparable or better results on synthetic and real datasets.

\textbf{Chapter~\ref{ch:carti-rank}} approaches to the caritification from a more sophisticated perspective.
Instead of creating a binary transaction database, \cartirank creates ranked neighborhood matrices.
%jand
%employs tile miners on them to detect cluster structures.
We study the properties of these ranked neighborhood matrices and propose an algorithm that exploits them to efficiently mine tiles, which are the representations of cluster structures.
We show that preserving the order of objects yields more robust results.

\textbf{Chapter~\ref{ch:carti-gui}} is about our tool \cartigui, which makes the neighborhood mining available for a wider audience.
\cartigui, the name of which stands for Visual Interactive Neighborhood Miner,
provides a user friendly interface that
combines an intuitive representation of neighborhoods with unsupervised algorithms.
Main focus of \cartigui is to show the relations of objects among different attributes,
so that a user can quickly get a grasp of the data.
Moreover, the interactivity enables the user to perform basic data manipulations and to create different views on the data.

\textbf{Chapter~\ref{ch:bigfim}} introduces two frequent itemset mining algorithms that are implemented on the Hadoop framework.
One of the key constraints of interactive data exploration settings is the reaction time.
Fortunately, heavy mining algorithms can be \emph{outsourced} to cloud services, so that the limited resources of the client can be better utilized.
In this manner, \declat and \bigfim are two parallel algorithms that can be run on cloud services.
We show that performance of mining algorithms in a MapReduce paradigm depends heavily on balanced distribution of data among the nodes.
We did exhaustive experiments and investigate the ways to distribute data and computational burden optimally.

\textbf{Chapter~\ref{ch:conclusions}} gives an overview of our contributions and concludes the thesis with an outlook for future possibilities.
