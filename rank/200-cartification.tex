\section{Subspace Clustering using Cartification}
\label{sec:rank-ordered}

This chapter improves the cartification approach for subspace clustering.
We start this section by summarizing the most recent cartification algorithm \clon as introduced in Chapter~\ref{ch:clon}~\cite{clon}.

Cartification operates on relational databases. A \textit{relational database} \DB is defined as a set of data objects $\{\vo_1,\ldots,\vo_n\}$.
Each data object $\vo_i$ consists of a vector defined over attributes $\Ats = \{A_1, A_2, \cdots, A_m\}$.
We are interested in finding subsets of attributes $S \subseteq \Ats$ and subsets of object identifiers $O\subseteq \{1,\ldots,n\}$ such that for each attribute $A\in S$, the values in the selected set of objects $O$ are similar.

We use $\dist(\voi,\voj)$ as the dissimilarity between objects \voi and \voj.
$|O|$ is the cardinality of set $O$.


\begin{algorithm}[tb]
	\begin{algorithmic}[1]
		\FORALL{$d \in \{ 1,\ldots, m\}$}
			%\FOR{$0 \rightarrow c$}
			\IF{$A_d\not\in S$}
				\STATE $C := $ \findclusters($O$, $a_d$)
				\FORALL{$O'\in C$}
					\IF{$|O'|\geq \mu $}
						\STATE Subspace Search ($S\cup \{A_d\}$, $O'$)
					\ENDIF
				\ENDFOR
			\ENDIF
		\ENDFOR

	\end{algorithmic}
	\caption{Subspace Search ($S$, $O$ )}
	\label{algo:subspacesearch}
\end{algorithm}

The Cartification algorithm is an instance of Algorithm~\ref{algo:subspacesearch}, where the algorithm is initialized with $S=\emptyset$ and $O=\{1,\ldots,n\}$.
The algorithm iteratively considers all attributes and identifies clusters in the attributes by using a function \findclusters.
The function \findclusters($O$, $A_d$) is the core of the algorithm and looks for good clusters for attribute $A_d$,
restricting the search to objects in set $O$.
For each resulting cluster the search recurses if the cluster is large enough.

Due to its recursive nature, Cartification can identify a large number of subspace clusters. Important aspects in how many clusters it identifies are the choice for the parameter $\mu$ and the exact implementation of function \findclusters.



In the version of Cartification proposed by Aksehirli et al.~\cite{clon}, \findclusters relies on a database of $k$-nearest neighborhoods.
The $k$-nearest neighborhood of one object in a database is the set of $k$  objects that are closest to it; we can construct such a neighborhood for all objects in the dataset.

%The key idea is that a good cluster must be part of many $k$-nearest neighborhoods in this neighborhood database.

%$k$-nearest neighborhoods have are been used widely for both supervised and unsupervised data analysis~\cite{knnclassification,SNNClustering}. The advantage of this approach is that it is less sensitive to scale or noise.

More formally, we can define the $k$-nearest neighborhood for one object as follows.

\begin{definition}[$k$-Nearest Neighborhood]
	Let $\textit{NN}_{k}(\voi)$ be the $k^{th}$ closest object to \voi, then the $k$-nearest neighborhood of \voi is defined as
	\[
	\knn{k}{\voi} = \{\voj | \dist(\voi, \voj) \le \dist(\voi, \textit{NN}_k(\voi))\}
		\]
	
\end{definition}
For example, the 6-nearest neighborhood of $\vo_{1}$ in Table~\ref{tab:example-db} is $\knn{6}{\vo_{1}} = \{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$.






A neighborhood database is created based on the $k$-nearest neighborhoods.

\begin{definition}[Neighborhood Database]
	\label{def:neighborhood-db}
    Given a parameter $k$, the 
    neighborhood data\-base is a vector
	$$
	(\knn{k}{\vo_{1}}, \knn{k}{\vo_{1}}, \ldots, \knn{k}{\vo_{n}}),
	$$
	where $\knn{k}{\vo_{i}}$ represents the set of neighbors of object $i$.
\end{definition}
The neighborhood database of the dataset in Table~\ref{tab:example-db} is shown in Table~\ref{tab:neighborhood-db}.

Note that the \emph{neighborhood database} is a \emph{transaction database}, as widely used in the domain of frequent itemset mining.

The idea behind cartification is that the objects that form clusters co-occur in each others' neighborhoods.
For example, objects 1, 2, and 3 are members of the same cluster and they co-occur in the neighborhoods of the objects 1, 2, 3, 4, 7, and 8, cf.\ Table~\ref{tab:neighborhood-db}.

Consequently, in cartification an {\em itemset mining}-like approach is used to identify sets of frequently co-occurring objects. In~\cite{clon}, this is done by looking for frequent sets of objects in the neighborhood database. Each such frequent set identifies a cluster, and is returned by the \findclusters function used in Algorithm 1.


\begin{table}[tb]
	%\caption{}
	\centering
	\subfloat[Database\label{tab:example-db}]{
		\begin{tabular}{|c|c|}
			\hline
			& $A_1$ \\ \hline
			$\vo_{1}$ & 3000 \\ \hline
			$\vo_{2}$ & 2000 \\ \hline
			$\vo_{3}$ & 2300 \\ \hline
			$\vo_{4}$ & 2600 \\ \hline
			$\vo_{5}$ & 7\\ \hline
			$\vo_{6}$ & 11 \\ \hline
			$\vo_{7}$ & 2100 \\ \hline
			$\vo_{8}$ & 3500 \\ \hline
			$\vo_{9}$ & 4 \\ \hline
			$\vo_{10}$& 16 \\ \hline
			$\vo_{11}$& 2 \\ \hline
			$\vo_{12}$& 1 \\ \hline
		\end{tabular}
	}
	\hspace{3em}
	\subfloat[Neighborhood Database\label{tab:neighborhood-db}]{
		\begin{tabular}{|c|c|}
			\hline
			$\knn{6}{\vo_1}$ & $\{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$ \\ \hline
			$\knn{6}{\vo_2}$ & $\{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$ \\ \hline
			$\knn{6}{\vo_3}$ & $\{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$ \\ \hline
			$\knn{6}{\vo_5}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
			$\knn{6}{\vo_6}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
			$\knn{6}{\vo_7}$ & $\{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$ \\ \hline
			$\knn{6}{\vo_8}$ & $\{\vo_1, \vo_2, \vo_3, \vo_4, \vo_7, \vo_8\}$ \\ \hline
			$\knn{6}{\vo_9}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
			$\knn{6}{\vo_{10}}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
			$\knn{6}{\vo_{11}}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
			$\knn{6}{\vo_{12}}$ & $\{\vo_5, \vo_6, \vo_9, \vo_{10}, \vo_{11}, \vo_{12}\}$ \\ \hline
		\end{tabular}
	}
	\caption{An example database of 12 data objects with one attribute and its neighborhood database}
\end{table}

One advantage of cartification is which clusters to be found are determined by the relative order of the objects;
the original distances in the database are not used directly.
This makes the approach less scale dependent.

An issue is however how to set $k$ properly: for a large value of $k$, the neighborhood of each object will be large; indeed, for $k=|O|$, all neighborhoods would be identical and would contain all objects in $O$.
Within the resulting database, we could potentially identify every subset of $O$ as a cluster.
If we set $k$ too low, on the other hand, we also limit the maximum size of clusters that can be found.
Finding a value of $k$ that is neither too low nor too high is hence not easy.
To address this problem, we propose a new approach in this chapter that operates on the ordered neighborhoods.

